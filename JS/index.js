$(document).ready(function () {
  /////open close mobile menu/////////

  var host = "http://localhost:8000/project/php";
  var userLang;
  var userLat;
  $(document).on("click", "#close_menu", toggle_menu);
  $(document).on("click", "#open_menu", toggle_menu);
  function toggle_menu(e) {
    $("#navbar").toggle();
    $("#close_menu").toggle();
    $("#open_menu").toggle();
    // alert(e.target.id);
    // if(e.target.id=="open_menu"){

    //   $("#mobileNav-logo").hide();
    // }

    // let menu = document.getElementById("navbar");

    // if (menu.style.display != "block") {

    //   menu.style.display = "block";
    //   document.getElementById("close_menu").style.display = "block";
    //   document.getElementById("open_menu").style.display = "none";
    // } else if (menu.style.display == "block") {
    //   menu.style.display = "none";

    //   document.getElementById("close_menu").style.display = "none";
    //   document.getElementById("open_menu").style.display = "block";
    // }
  }

  //////Toggle Search Box ON Navigation on scroll////////
  window.onscroll = function (e) {
    // if ($(window).width() < 789){
    //   if (window.scrollY >= 201) {

    //     document.getElementById("mobileNav-logo").style.display =
    //       "flex";
    //   } else if (window.scrollY <= 200) {

    //     document.getElementById("mobileNav-logo").style.display =
    //       "none";
    //   }

    // }

    if (window.scrollY >= 400) {
      document.getElementById("scrol-top").style.display = "block";
    }
    if (window.scrollY <= 400) {
      document.getElementById("scrol-top").style.display = "none";
    }
  };

  ////////////Confirmation Box//////////////
  function Confirmation(title, message, yes, cancel, succssAction) {
    var content =
      "<div id='confirm' class='confirm'>" +
      "<div class='dialog'><div class='confirm-title'>" +
      " <h3> " +
      title +
      " </h3> " +
      "  <span id='close-confirm'>&times;</span>" +
      "</div>" +
      "<div class='confirm-message'>" +
      " <p> " +
      message +
      " </p> " +
      "</div>" +
      "<div class='confirm-options'>" +
      " <button class='button-sec confirm-btn confirmAction'>" +
      yes +
      "</button> " +
      " <button class='button-primary cancel-btn cancelAction'>" +
      cancel +
      "</button> " +
      "</div>";
    "</div>" + "</div>";
    $("body").prepend(content);

    let confirmBox = document.getElementById("confirm");
    window.onclick = function (event) {
      if (event.target == confirmBox) {
        $("#confirm").fadeOut(400, function () {
          $(this).remove();
        });
      }
    };
    $(".confirmAction").click(function () {
      succssAction();
      $(this)
        .parents(".confirm")
        .fadeOut(400, function () {
          $(this).remove();
        });
    });
    $(".cancelAction, #close-confirm").click(function () {
      $(this)
        .parents(".confirm")
        .fadeOut(400, function () {
          $(this).remove();
        });
    });
  }
  ///////////////top drop down menue///////
  $("#topusername").click(function () {
    $("#dropdown").toggle();
  });
  $(window).click(function (event) {
    if (event.target.id != "topusername" && event.target.id != "dropdown") {
      $("#dropdown").hide();
    }
  });

  ///////Login Form MOdal/////////
  $(".login-modal").click(function () {
    $("#popupbg").fadeIn(300);
  });
  $("#close-popup").click(function () {
    $(".modal").fadeOut(400);
  });
  $(".modal").click(function (e) {
    if (e.target !== this) return;
    $(".modal").fadeOut(400);
  });

  //////////Message BOxes//////////

  function addWarningMessage(message) {
    let content = `<div  class="message-box warning-message">
  <div><i class="fas fa-exclamation-triangle"></i>&nbsp;&nbsp;&nbsp;
  <span>
  ${message}
  </span>
 </div>
 </div>
 `;
    $(".message-box").remove();
    $("body").prepend(content);
    $(".warning-message").slideDown(200).delay(3000).fadeOut();
  }

  function addErrorMessage(message) {
    let content = `<div class="message-box error-message">
   <div><i class="fas fa-skull-crossbones"></i>
   &nbsp;&nbsp;&nbsp;<span>${message}</span>
  </div>
  </div>`;
    $(".message-box").remove();
    $("body").prepend(content);
    $(".error-message").slideDown(200).delay(3000).fadeOut();
  }
  function addSuccessMessage(message) {
    let content = `<div  class="message-box success-message">
   <div><i class="fas fa-check-circle"></i>
   &nbsp;&nbsp;&nbsp;<span>${message}</span>
  </div>
  </div>`;
    $(".message-box").remove();
    $("body").prepend(content);
    $(".success-message").slideDown(200).delay(3000).fadeOut();
  }

  //////////////////////Service POst Thumbnail Hover Effect/////////////////////

  $(document).on("mouseenter", ".service-card-thumbnail", function () {
    $(this).find(".service-card-hover").fadeIn();
    $(this).find(".service-thumb").addClass("zoom-thumbnail");
  });
  $(document).on("mouseleave", ".service-card-thumbnail", function () {
    $(this).find(".service-card-hover").hide();
    $(this).find(".service-thumb").removeClass("zoom-thumbnail");
  });

  /////////////To Validate The Singup Form/////////

  $("#singupbtn").click(function () {
    let submit = true;
    let pass = document.forms["signupForm"]["password"].value;
    let cpass = document.forms["signupForm"]["cpassword"].value;
    let email = document.forms["signupForm"]["email"].value;
    let fname = document.forms["signupForm"]["fname"].value;
    let lname = document.forms["signupForm"]["lname"].value;

    if (fname.length < 2) {
      document.getElementById("fname").style.border = "1px solid red";
      submit = false;
    }
    if (lname.length < 2) {
      document.getElementById("lname").style.border = "1px solid red";
      submit = false;
    }

    /////////Applay alll conditions////
    if (!email.includes("@") && !email.includes(".")) {
      document.getElementById("lable-email").innerText =
        "Kindly enter correct email";
      document.getElementById("email").style.border = "1px solid red";
      submit = false;
    }
    if (pass != cpass) {
      document.getElementById("lable-cpass").innerText = "Password not matched";
      document.getElementById("cpassword").style.border = "1px solid red";
      submit = false;
    }

    if (pass.length < 6) {
      document.getElementById("lable-pass").innerText =
        "Password must be more than 6 characters";
      document.getElementById("password").style.border = "1px solid red";
      document.getElementById("cpassword").style.border = "1px solid red";
      submit = false;
    }

    if (submit != false) {
      let form = new FormData(document.getElementById("signupForm"));
      $.ajax({
        url: "Database/CheckExistingUser.php",
        type: "POST",
        data: form,
        contentType: false,
        processData: false,
        success: function (data) {
          if (data == 0) {
            $("#lable-email").html("Account alredy exist with this Email");
          }
          if (data == 1) {
            $("#lable-email").html("");
            $("#signupForm").submit();
          }
        },
      });
    }
  });

  //////////Email Verification////////

  $("#confirmEmail").on("click", function () {
    let otp = $("#verify-email-code").val();

    $.ajax({
      url: "Database/VerifyCode.php",
      type: "POST",
      data: {
        otp: otp,
      },
      success: function (data) {
        console.log(data);
        if (data == 0) {
          window.location.href = host;
        }
        if (data == 1) {
          addErrorMessage("Wrong Confirmation Code");
        } else {
          addErrorMessage("An Error Occured");
        }
      },
    });
  });

  ///////confirm Password Validation//////////
  function confirmPassword() {
    let pass = document.forms["signupForm"]["password"].value;
    let cpass = document.forms["signupForm"]["cpassword"].value;

    if (pass.length > 6) {
      if (pass == cpass) {
        document.getElementById("password").style.border = "1px solid green";
        document.getElementById("cpassword").style.border = "1px solid green";
      }
    }
  }

  ////////////Add POst Thubmail Ajax////////////

  $(document).on("click", "#upload-img", function () {
    $("#thumbnail").click();
  });
  $("#thumbnail").change(function () {
    let form = new FormData(document.getElementById("add-post"));
    $.ajax({
      url: "../php/Database/UploadThumbnail.php",
      type: "POST",
      data: form,
      contentType: false,
      processData: false,
      success: function (data) {
        if (data == 0) {
          $("#thumbnail").val(null);
          addErrorMessage("Only Images Are Allowed To Upload");
        } else if (data == 1) {
          $("#thumbnail").val(null);
          addErrorMessage("Error! Image Size Is Greater Than 2 MB");
        } else {
          $(".message-box").remove();
          document.getElementById("thumbnail-tab").innerHTML = data;
        }
      },
    });
  });
  $(document).on("click", "#remove-thumbnail", function () {
    let form = new FormData(document.getElementById("add-post"));
    $.ajax({
      url: "../php/Database/DeleteThumbnail.php",
      type: "POST",
      data: form,
      contentType: false,
      processData: false,
      success: function (data) {
        $("#thumbnail").val(null);
        document.getElementById("thumbnail-tab").innerHTML = data;
      },
    });
  });

  ///////////Save Post///////////////////

  $("#save-post").on("click", function (e) {
    e.preventDefault();
    let form = new FormData(document.getElementById("add-post"));
    $.ajax({
      url: "../php/Database/SavePost.php",
      type: "POST",
      data: form,
      contentType: false,
      processData: false,
      success: function (data) {
        if (data == 0) {
          addWarningMessage("Kindly Fill All Blanks");
        }
        if (data == 2) {
          addErrorMessage(" An Error Occured Kindly Try Again");
        }
        if (data == 1) {
          addSuccessMessage("post added sucessfuly");
          document.getElementById("add-post").reset();

          document.getElementById("thumbnail-tab").innerHTML = `
        <img id="upload-img"src="../images/upload-img.png" onclick="startclick()"style="margin: auto;" alt=" "/>`;
          // window.location.href=`${host}/profile.php`;
        }
      },
    });
  });
  ////////  Singup /////////////////////////

  // $("#singupbtn").on("click", function () {
  //   let form = new FormData(document.getElementById("signupForm"));
  //   $.ajax({
  //     url: "Database/AddSignUpData.php",
  //     type: "POST",
  //     data: form,
  //     contentType: false,
  //     processData: false,
  //     success: function (data) {},
  //     error: function () {
  //       addErrorMessage("error 404");
  //     },
  //   });
  // });

  ////////////////Login With Gmail////////////////
  function startApp() {
    gapi.load("auth2", function () {
      // Retrieve the singleton for the GoogleAuth library and set up the client.
      auth2 = gapi.auth2.init({
        client_id:
          "1028967532523-7a1h4tnbmk0atjbbpt1bf1hcfhg36ncq.apps.googleusercontent.com",
        cookiepolicy: "single_host_origin",
        scope: "profile",
        // Request scopes in addition to 'profile' and 'email'
        //scope: 'additional_scope'
      });
      if (document.getElementById("login-with-gmail-popup")) {
        attachSignin(document.getElementById("login-with-gmail-popup"));
      }
      if (document.getElementById("login-with-gmail")) {
        attachSignin(document.getElementById("login-with-gmail"));
      }
      if (document.getElementById("singup-with-gmail")) {
        attachSignin(document.getElementById("singup-with-gmail"));
      }
    });
  }

  function attachSignin(element) {
    var auth2 = gapi.auth2.getAuthInstance();
    auth2.signOut().then(function () {
      console.log("User signed out.");
    });
    auth2.attachClickHandler(
      element,
      {},
      function () {
        let tokenId = gapi.auth2
          .getAuthInstance()
          .currentUser.get()
          .getAuthResponse().id_token;
        $.ajax({
          url: "Database/SinginWithGmail.php",
          type: "POST",
          data: {
            token: tokenId,
          },
          success: function (data) {
            console.log(data);
            if (data == 0 || data == 1) {
              window.location.href = host;
            }
            if (data == 1) {
              window.location.href = host + "/admin";
            }
            if (data == 2) {
              addWarningMessage(
                "Account Is Not Connected With Gmail Use Your Email And Password To Login."
              );
            }
            if (data == 3) {
              window.location.href = host + "/profile-settings.php";
            }
            if (data == 4) {
              addErrorMessage("Invalid Gmail Token");
            }
            if (data == 5) {
              addErrorMessage("Gmail Token Is Empty");
            }
          },
          error: function () {
            addErrorMessage("error 404");
          },
        });
      },
      function (error) {
        addErrorMessage("Singup Failed");
      }
    );
  }
  startApp();

  ////////////////Show POsts Filter///////////

  $("#applyFilters").on("click", function () {
    console.log("In Function");
    let orderId = $("#timeList").val();
    let priceId = $("#priceList").val();
    let city = $("#cityList").val();
    let catid = $("#CategoryList").val();
    let searchParams = new URLSearchParams(window.location.search);
    let searchq;
    let url = " ";
    if (searchParams.has("search") === true) {
      searchq = searchParams.get("search");
      url =
        "../php/services.php?search=" +
        searchq +
        "&categoryId=" +
        catid +
        "&cityId=" +
        city +
        "&priceId=" +
        priceId +
        "&orderId=" +
        orderId;
    } else {
      url =
        "../php/services.php?categoryId=" +
        catid +
        "&cityId=" +
        city +
        "&priceId=" +
        priceId +
        "&orderId=" +
        orderId;
    }

    window.location.href = url;
  });

  //////Scroll To Top/////
  $("a[href='#top']").click(function () {
    $("html, body").animate({
      scrollTop: 0,
    });
    return false;
  });

  ///////////////Leaflet Maps/////////////////////

  if (document.getElementById("profile-settings-map")) {
    var lang = 0;
    var lat = 0;

    $.ajax({
      url: "../php/Database/GetLocation.php",
      type: "POST",
      dataType: "json",
      success: function (data) {
        if (data[0].userLang == "") {
          lang = 69.3451;
        } else {
          lang = data[0].userLang;
        }
        if (data[0].userLat == "") {
          lat = 30.3753;
        } else {
          lat = data[0].userLat;
        }
        let map = L.map("profile-settings-map").setView([lat, lang], 15);
        L.tileLayer("https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png", {
          attribution:
            '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors',
        }).addTo(map);
        var marker = L.marker([lat, lang], {
          draggable: false,
        }).addTo(map);
        map._handlers.forEach(function (handler) {
          handler.disable();
        });
        $(document).on("click", "#edit-location-btn", function (e) {
          map._handlers.forEach(function (handler) {
            handler.enable();
          });
          $("#map-layer").hide();
          $("#canel-location-btn").show();
          $("#current-location-btn").show();
          $("#save-location-btn").show();
          $(this).hide();
          marker.dragging.enable();
          map.on("click", function (e) {
            marker.setLatLng(e.latlng);
          });
        });
        $(document).on("click", "#current-location-btn", function (e) {
          map._handlers.forEach(function (handler) {
            handler.enable();
            getLocation();
            if (userLat > 0 && userLang > 0) {
              var newLatLng = new L.LatLng(userLat, userLang);
              marker.setLatLng(newLatLng);

              map.flyTo([userLat, userLang], 16);
            } else {
              alert("Cant Detect Location! Kindly Allow Permissions");
            }
          });
        });
        $(document).on("click", "#canel-location-btn", function () {
          map._handlers.forEach(function (handler) {
            handler.disable();
          });
          $("#map-layer").show();
          $("#canel-location-btn").hide();
          $("#current-location-btn").hide();
          $("#save-location-btn").hide();
          $("#edit-location-btn").show();
          var newLatLng = new L.LatLng(lat, lang);
          marker.setLatLng(newLatLng);

          map.flyTo([lat, lang], 16);
          marker.dragging.disable();
        });
        $(document).on("click", "#save-location-btn", function () {
          map._handlers.forEach(function (handler) {
            handler.disable();
          });
          lat = marker.getLatLng().lat;
          lang = marker.getLatLng().lng;
          $("#lat").val(lat);
          $("#lang").val(lang);
          marker.dragging.disable();
          $("#map-layer").show();
          $("#canel-location-btn").hide();
          $("#save-location-btn").hide();
          $("#current-location-btn").hide();
          $("#edit-location-btn").show();
        });
      },
    });
  }

  //////////////Update Profile Setting//////////////
  $("#saveSetting").on("click", function (e) {
    e.preventDefault();
    let form = new FormData(document.getElementById("Setting"));
    $.ajax({
      url: "../php/Database/ProfileSettingsData.php",
      type: "POST",
      data: form,
      contentType: false,
      processData: false,
      success: function (data) {
        console.log(data);
        if (data == 0) {
          addWarningMessage("Kindly Fill All (*)Required Fields");
        }
        if (data == 2) {
          addErrorMessage(" An Error Occured Kindly Try Again");
        }
        if (data == 4) {
          addErrorMessage(" File Format Not Supported");
        }
        if (data == 5) {
          addErrorMessage(" Image Size Should Be Lessthan 2MB");
        }
        if (data == 1) {
          addSuccessMessage("Settings Update Sucessfuly");
        }
      },
    });
  });

  //////////////Post Page Map///////////////

  if (document.getElementById("post-location")) {
    var lang = 0;
    var lat = 0;
    let searchParams = new URLSearchParams(window.location.search);
    let id = searchParams.get("id");

    $.ajax({
      url: "../php/Database/GetLocation.php",
      type: "POST",
      data: { serviceId: id },
      dataType: "json",
      success: function (data) {
        console.log(data);
        if (data[0].userLang == "") {
          lang = data[0].cityLang;
        } else {
          lang = data[0].userLang;
        }
        if (data[0].userLat == "") {
          lat = data[0].cityLat;
        } else {
          lat = data[0].userLat;
        }
        let map = L.map("post-location").setView([lat, lang], 15);
        L.tileLayer("https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png", {
          attribution:
            '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors',
        }).addTo(map);
        var locationMarker = L.marker([lat, lang], {
          draggable: false,
        }).addTo(map);

        var greenIcon = new L.Icon({
          iconUrl: 'https://raw.githubusercontent.com/pointhi/leaflet-color-markers/master/img/marker-icon-2x-green.png',
          shadowUrl: 'https://cdnjs.cloudflare.com/ajax/libs/leaflet/0.7.7/images/marker-shadow.png',
          iconSize: [25, 41],
          iconAnchor: [12, 41],
          popupAnchor: [1, -34],
          shadowSize: [41, 41]
        });

        var currentlocationMarker = L.marker([userLat, userLang], {
          icon: greenIcon,
          draggable: false,
        }).addTo(map);
      },
    });
  }

  ////////////////////Get User Location///////////////////

  function getLocation() {
    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition(showPosition);
    } else {
      x.innerHTML = "Geolocation is not supported by this browser.";
    }
  }
  function showPosition(position) {
    userLang = position.coords.longitude;
    userLat = position.coords.latitude;

    $.ajax({
      url: "../php/Database/SaveUserLocation.php",
      type: "POST",
      data: { userLang: userLang, userLat: userLat },
      success: function (data) {},
    });
  }

  getLocation();

  /////////////Search Quries///////////

  $("#searchBtn").on("click", function () {
    let search = $("#search-box").val();
    if (search != "") {
      window.location.href = " ../php/services.php?search=" + search;
    } else {
      addWarningMessage("Empty Filed Cant Be Search!");
    }
  });
  let searchQuery = new URLSearchParams(window.location.search);
  if (searchQuery.has("search") === true) {
    let searchq = searchQuery.get("search");
    $("#search-box").val(searchq);
  }

  //////////ForGot Password////////////////
  $("#send-new-pass-code").on("click", function (e) {
    e.preventDefault();
    let email = $("#email-pass-reset").val();
    if (email != "") {
      $.ajax({
        url: "../php/Database/ForgotPassword.php",
        type: "POST",
        data: { email: email },
        success: function (data) {
          console.log(data);
          if (data == 0) {
            window.location.href = "../php/forgot-password-enter-code.php";
          }
          if (data == 1) {
            addWarningMessage("Email not Found");
          }
        },
      });
    }
  });

  $("#confirm-pass-Email").on("click", function () {
    let otp = $("#verify-pass-email-code").val();

    $.ajax({
      url: "Database/VerifyCode.php",
      type: "POST",
      data: {
        otp: otp,
      },
      success: function (data) {
        console.log(data);
        if (data == 0) {
          window.location.href = "../php/new-password.php";
        }
        if (data == 1) {
          addErrorMessage("Wrong Confirmation Code");
        } else {
          addErrorMessage("An Error Occured");
        }
      },
    });
  });

  ///////////Change Password////////////
  $(document).on("click", "#changePasswordBtn", function () {
    $("#Password-edit").fadeIn(300);
  });

  $("#changePassword").on("click", function () {
    let form = new FormData(document.getElementById("Change_Password"));
    $.ajax({
      url: "../php/Database/ChangePassword.php",
      type: "POST",
      data: form,
      contentType: false,
      processData: false,
      success: function (data) {
        if (data == "1") {
          addSuccessMessage("Password Changed Sucessfully");
          document.getElementById("Change_Password").reset();
          $(".modal").fadeOut(400);
        } else if (data == "0") {
          addWarningMessage("Kindly Fill All Blanks");
        } else if (data == "4") {
          addWarningMessage("Confirm Password Does Not Match");
        } else if (data == "2") {
          addErrorMessage("An Error Occured Kindly Try Again");
        } else if (data == "3") {
          addErrorMessage("Invalid Current Password");
        }
      },
      error: function () {
        addErrorMessage("error 404");
      },
    });
  });

  $(document).on("click", ".cancel", function () {
    $(".modal").fadeOut(400);
  });

  ////////////Delete Posts//////////////
  $(document).on("click", ".post-delete", function () {
    let targetElement = this;
    let postid = $(this).data("postid");
    let postTitle = $(this).data("title");
    function succssAction() {
      $.ajax({
        url: "Database/DeleteService.php",
        type: "POST",
        data: { id: postid },
        success: function (data) {
          if (data == 2) {
            addErrorMessage(" An Error Occured Kindly Try Again");
          }
          if (data == 1) {
            addSuccessMessage(" Post Deleted Successfully");
            $(targetElement).closest(".postsBoxes").fadeOut();
          }
        },
        error: function () {
          addErrorMessage("error 404");
        },
      });
    }
    Confirmation(
      "Delete Post",
      "Are you sure you want to Delete ?<b>" + postTitle + "<b>",
      "Yes",
      "Cancel",
      succssAction
    );
  });
}); ///////Document Ends/////
