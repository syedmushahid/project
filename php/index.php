
<?php include "config.php"?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?php echo $title?>
</title>
    <?php
echo $links;
?>
</head>
<body>
<?php
include"header.php";
?>
 
<main>
    <div class="col-12 landing-top container">
<?php 
$sql="Select * from tagline";
$result=mysqli_query($connection,$sql);
if(mysqli_num_rows($result)>0){
$row=mysqli_fetch_assoc($result);

?>
        <div class="top-tag col-md-6">
            <div>
<h1 class="top-tag-heading"><?php echo $row['Heading']?></h1>

<p><?php echo $row['paragraph']?></p>
<button onclick="window.location.href='signup.php'" class="button-sec primary-border" >
 Join For Free
</button>


</div></div>
        <div class="top-image col-md-6">
<div>
    <img width="100%" src="<?php echo "../Uploads/".$row['image']?>" alt="Top Image">
</div>
        </div>
       <?php 
       }
    ?>
</div>
<br/>
<br/>
<div class="container-fluid sec-col">
<div class="container home_categories">
<h2>Explore The Services</h2>
<?php 
$query="select * from homepagecat";
$getResults=mysqli_query($connection,$query);
if(mysqli_num_rows($getResults)){
while($cat=mysqli_fetch_assoc($getResults)){

?>
<div  onclick="location.href='<?php echo $cat['link']?>';" class="category col-md-2 col-xs-3"><?php echo $cat['icon']?> <h3><?php echo $cat['title']?></h3></div>

<?php  
}
}

?>
<!-- 
<div  onclick="location.href='#';" class="category col-md-2 col-xs-3"><i class="fas fa-cogs"></i> <h3>Mechanic</h3></div>
<div  onclick="location.href='#';" class="category col-md-2 col-xs-3"><i class="fas fa-plug"></i><h3>Electrician</h3></div>
<div  onclick="location.href='#';" class="category col-md-2 col-xs-3"><i class="fas fa-shower"></i><h3>Plumber</h3></div>
<div  onclick="location.href='#';" class="category col-md-2 col-xs-3"><i class="fas fa-hammer"></i><h3>Carpanter</h3></div>
<div  onclick="location.href='#';" class="category col-md-2 col-xs-3"><i class="fas fa-brush"></i><h3>Painter</h3></div>
<div  onclick="location.href='#';" class="category col-md-2 col-xs-3"><i class="fas fa-user"></i><h3>Labour</h3></div>
<div  onclick="location.href='#';" class="category col-md-2 col-xs-3"><i class="fas fa-desktop"></i><h3>Pc Repair</h3></div>
<div  onclick="location.href='#';" class="category col-md-2 col-xs-3"><i class="fas fa-mobile-alt"></i><h3>Mobile Repair</h3></div>
<div  onclick="location.href='#';" class="category col-md-2 col-xs-3"><i class="fab fa-pagelines"></i><h3>Gardner</h3></div>
<div  onclick="location.href='#';" class="category col-md-2 col-xs-3"><i class="fas fa-mobile-alt"></i><h3>Mobile Repair</h3></div>
<div  onclick="location.href='#';" class="category col-md-2 col-xs-3"><i class="fab fa-pagelines"></i><h3>Gardner</h3></div>
<div  onclick="location.href='#';" class="category col-md-2 col-xs-3"><i class="fas fa-angle-double-right"></i><h3>More</h3></div> -->

</div>
</div>

<br/>
<br/>
<!-- content part start -->
<!-- <div class="container col-12">

    <div class="col-md-6">
        <h2>Professional And Experience Workers</h2>
<p>
    Tired of having to walk around your town centre in search of that one amazing person with the perfect skillset for your specific needs? Our dynamic site is loaded with vetted labor force ready to be ushered into each and every home, restaurant or other business. Giving you more life to focus on building out your business while we wage war on the practicality crisis.
    <br>
    <br>
    Mazdoor.pk is the best place to hire a local mazdoor for those who need help around their home or workplace. You can hire any number of workers to do quick and big jobs like cleaning, gardening, fixing things like plumbing and electrical problems, painting and replacing windows as well as many other projects too!
   </p>
    </div>
    <div class="col-md-6">
        <img src="../images/img1.jpg" alt="worker image" style="margin: auto;" width="90%">
    </div>
</div>
<br/>
<br>
<br>
<div class="container-fluid sec-col">
    <div style="padding-top: 20px; padding-bottom: 20px;" class="container col-12">
        <div class="col-md-6">
            <img src="../images/img2.jpg" alt="worker image" style="margin: auto;" width="90%">
 
        </div>
        <div class="col-md-6">
            <h2>Save Your Time And Money </h2>
            <p>
                <ul>
                    <li>Find the right worker for your work with mazdoor.pk</li>
                   <br>
                    <li>Access the right workforce to get results done with us.</li>
                    <br>
                    <li>In 15 Seconds you can post your services and have professionals deals to do the job. No posting fees - just pay for what you use.</li>
                   <br>
                   <li>Get Workers in very resonable price.</li>
                   <br>
                    <li>We Encourage and promote professional workers</li>
                </ul>
            </p>
        </div>
    </div>
</div> -->
<?php include "content.txt"?>
<!-- content part ends -->
</main>

<?php
include"footer.php";
?>
</body>

</html>