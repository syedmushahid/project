<?php
include "config.php";
$id = mysqli_real_escape_string($connection,$_GET['id']);


$updateView = "Update services set views =views+1 where Service_Id = {$id}";
mysqli_query($connection, $updateView);

$getposts = "SELECT user.id as userId, CONCAT( user.First_Name,' ' ,user.Last_Name) AS Name, media.Profile_Photo AS Profile_Photo,city.City_Name as cityName,services.Price as Fee,user.Address as userAddress,services.Description as PostDescription,user.bio as Bio,
category.Category_Name AS Category, user.Phone_Number as Phone_Number,user.email as Email, services.Service_Id As service_id,services.Worker_Id AS workerId,
services.Title AS Title ,services.thumbnail As thumbnail, 
services.posted_time AS posted_time
FROM services LEFT JOIN category ON services.Category_Id=category.Category_Id 
INNER JOIN user ON services.Worker_Id=user.id left JOIN media on services.Worker_Id=media.User_Id  LEFT JOIN city on services.City_Id = city.City_Id where services.Service_Id=$id";
$postsresult = mysqli_query($connection, $getposts);

?>
<?php if (mysqli_num_rows($postsresult) > 0) {

    while ($post = mysqli_fetch_assoc($postsresult)) {
?>
        <!DOCTYPE html>
        <html lang="en">

        <head>
            <meta charset="UTF-8">
            <meta http-equiv="X-UA-Compatible" content="IE=edge">
            <meta name="viewport" content="width=device-width, initial-scale=1.0">
            <title><?php echo $post['Title'] . " | " . $title ?></title>
            <?php echo $links ?>
        </head>

        <body>
            <?php include "header.php" ?>
            <main>
         
                <div class="container-fluid profile-card-background">
                </div>
                <div class="container">
                    <div class="profile-card col-12">
                        <div class="profile-photo col-lg-12">
                            <?php if ($post['Profile_Photo'] == "") {
                            ?>
                                <img src="https://png.pngtree.com/png-vector/20190223/ourlarge/pngtree-profile-line-black-icon-png-image_691051.jpg" alt="profile" />
                            <?php } else {


                            ?>
                                <img src=<?php echo$post['Profile_Photo'] ?> alt="profile" />
                            <?php } ?>
                        </div>
                        <div class=" col-lg-10 col-md-11 profile-card-body">
                            <div class="col-12">
                                <div class="col-md-12 profile-name">
                                   <a href="<?php echo $hostname."profile.php?id=".$post['userId'];?>"><h1><?php echo$post['Name'] ?></h1></a> 
                                </div>
                                <div class="col-md-12 profile-btns"> <?php

                                   if (isset($_SESSION['user_id'])) {
                                   if ($post['workerId'] == $_SESSION['user_id']) { ?>
                                            <a href="profile-settings.php"><button class="contact-number button-sec">Edit<i class="fas fa-edit"></i></button></a>
                                        <?php
                                                                            }

                                        ?>

                                        <button class="contact-number button-sec"><i class="fas fa-phone"></i>&nbsp;&nbsp;<?php echo $post['Phone_Number'] ?></button>

                                    <?php } else {

                                    ?>
                                        <a class="login-modal"> <button class="contact-number button-sec"><i class="fas fa-phone"></i>Show Phone Number</button></a>
                                    <?php
                                                                        } ?>
                                </div>


                            </div>


                            <p style="text-align: center;"><?php echo$post['Bio'] ?></p>
                            <div class="profile-address col-12">

                                <p> <i class="fas fa-envelope ProfileIcon"> </i>&nbsp;&nbsp;&nbsp;<?php echo$post['Email'] ?></p>&nbsp;
                                <p><i class="fas fa-map-marker-alt ProfileIcon"></i>&nbsp;&nbsp;&nbsp; <?php echo$post['userAddress'] ?></p>&nbsp;
                                <p>
                                    <i class="fas fa-user-tie ProfileIcon"></i>&nbsp;&nbsp;&nbsp;<?php echo$post['Category'] ?>
                                </p>
                            </div>
                        </div>

                    </div>

<div class="col-12 service-details">
    <h1 style="padding-left:20px"class="col-12"><?php echo $post['Title']; ?></h1>
    <div class="col-md-7">
<div class="col-12 postThumbnail">
<img src="../Uploads/<?php echo $post['thumbnail'];?>" alt="thumbnail">
<h3 class="col-12">
 More Details   
</h3>

<p><?php echo $post['PostDescription']?></p>
</div>

    </div>
    <div style="padding:20px;border: 1px solid rgba(0, 0, 0, 0.281)" class="col-md-5">
    <div class="col-12"><h3>Visiting Fee : <?php echo $post['Fee'];?> /RS</h3></div>
    <div class="col-12"><h3> <h3>Address : <?php echo $post['userAddress'];?></h3></div>
    <div class="col-12"><h3>City : <?php echo $post['cityName'];?></h3></div>
    <div class="col-12"><h3>Category : <?php echo $post['Category'];?></h3></div>
    <div class="col-12"><h3>Posted on : <?php echo $post['posted_time'];?></h3></div>
    <div class="col-12"><h3>Location :</h3> <p>You : <div style="width:30px;"><img style="width:60%"src="https://raw.githubusercontent.com/pointhi/leaflet-color-markers/master/img/marker-icon-2x-green.png" alt=""></div></p> <p><?php echo$post['Name'];?> : <div style="width:30px;"><img style="width:60%"src="https://raw.githubusercontent.com/pointhi/leaflet-color-markers/master/img/marker-icon-2x-blue.png" alt=""></div></p></div>
    <div class="col-12" style="height: 350px;" id="post-location">
</div>
    </div>


      
        
        </div>
          
          
        </div>

            </main>

            <?php
            include "footer.php";
            ?>
        </body>

        </html>

<?php
    }
} else {
    header("location:{$hostname}404-error.php");
}
?>