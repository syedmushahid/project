<?php 
     include "config.php";
 if (session_status() === PHP_SESSION_NONE) {
    session_start();
    if (!isset($_SESSION['otp']) || empty($_SESSION['otp'])) {
        header("location:{$hostname}");
        die();
    }
}


?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Verify Email | <?php echo $title ?></title>
    <?php echo $links ?>
</head>

<body>
    <?php include "header.php"?>

    <main>
        <div class="col-lg-4 col-md-6 col-xs-10 forms-box">
            <h2>Kindly Verify Email To Login</h2>
            <p>Enter The Code That Has Been Sent To Your Email.</p>
            <input class="input-field forgot-pass" type="Email" id="verify-email-code" placeholder="Enter Code"/>

            <a href="Modules/SendOtp.php"><button id="resendOtp" class="button-primary">Resend</button></a> <button id="confirmEmail" class="button-sec">Confirm</button>
            
            <br>
            <br>
        </div>

    </main>
    <footer>

    </footer>
    <?php include "footer.php" ?>
</body>
<?php echo $script?>
</html>