<?php
include "config.php";
include "Modules/GetTotalnumbers.php";
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Users | <?php echo $title ?></title>
    <?php echo $links ?>
</head>

<body>
    <div class="col-12">
        <div class="col-lg-2 col-md-3 col-xs-7>
            <?php include "menu.php" ?>
        </div>
     <div class="col-md-9 col-lg-10" >
     <?php include "adminheader.php"?>



<div class="col-12 mid-boxes" >
    <div style="margin-top:30px" class="col-md-5 searchBox">
          <input id="user-query" class="input-field"  type="text"><button id="search-user">Search</button>
        </div>
</div>

<div id="user-table" class="col-sm-11 box-bg" style="margin-left: auto;margin-right: auto;">
<?php include "../Database/LoadUsers.php"?>
</div>
</div>

     </div>
    
     </div>    
      <?php
      
      ?>
</body>
<?php echo $script?>

</html>