<?php
include "config.php";
if (session_status() === PHP_SESSION_NONE) {
    session_start();
}
if(!isset($_SESSION['user_id']))
{
    header("location:{$hostname}");
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Team Members | <?php echo $title ?></title>
    <?php echo $links ?>
</head>

<body>
    <div class="col-12">
        <div class="col-lg-2 col-md-3 col-xs-7">
            <?php include "menu.php" ?>
        </div>
     <div class="col-md-9 col-lg-10" >
     <?php include "adminheader.php"?>


<div class="col-12 mid-boxes">

 <!-- Choose Member  -->
<div class= "col-12">

      <?php
if($_SESSION['user_role'] != 2)
{
    ?>
   <div class="col-12 mid-boxes">
<div id="popupBtn" class="col-md-2 col-sm-5 top-result  mousePointer"><a><i style="font-size: 30px;" class="fas fa-plus"></i><br><br>Member</a>
</div>
<div class="col-md-5 searchBox">
          <input id="member-query" class="input-field"  type="text"><button id="search-member">Search</button>
        </div>
</div>
    <?php
}
?>
    
      <div id="table-form" class="col-md-3" style="margin:auto">
        <form>
          Select Member : <select style="padding:10px" id="get-user-table">
                          <option disabled selected value="">Select Member</option>
                          <option  value="0">Admin</option>
                          <option  value="1">Manager</option>
                          <option  value="2">Moderator</option>

                        </select>
        </form>
</div>

      <div id="table-data" class="col-md-9 col-lg-10 box-bg">
        <?php include "../Database/LoadTeamMember.php"?>
      </div>
  
</div> <!-- Choose Member End  -->

</div>

</div>
     </div>
    </div>





<div id="popupbg" class="modal">
  <div class="modal-content">
    <span class="close-popup">&times;</span>
    
    <h1 class="box-top-h">Add Member</h1>

<form id="AddMember" class="signup-form col-12">
    <input  name="fname" class="fname" id="fname" type="text" placeholder="First Name">
    <input  name="lname" class="lname" id="lname" type="text" placeholder="Last Name">
    <input class="input-field" id="email" name="email" type="email" placeholder="Enter Email" >
    <input class="input-field" id="password" name="password" type="password" placeholder="Enter Password">
    <input class="input-field" name="cpassword" id="cpassword" type="password" placeholder="Confirm Password" onkeyup="confirmPassword()">
   <select name="role" class="input-field">
       <option disabled selected >Member Role?</option>
       <?php
       if($_SESSION['user_role'] == 0)
       {
       ?>
       <option  value="0">Admin</option>
       <?php
       }
       if($_SESSION['user_role'] == 0)
       {
       ?>
       <option  value="1">Manager</option>
       <?php
       }
       if($_SESSION['user_role'] == 0 || $_SESSION['user_role'] == 1)
       {
       ?>
       <option  value="2">Moderator</option>
       <?php
       }
       ?>
   </select>
</form>
    <button class="button-sec" id="addMember">Add</button>
    <button class="button-primary cancel">Cancel</button>
  </div>
</div>








<div id="edit-userRole-modal" class="modal">
  <div class="modal-content">
    <span class="close-popup">&times;</span>
    
    <h2 class="box-top-h">Update Member Role</h2>

<form id="UpdateUserRole">
    <select name="role" id="userRole" class="input-field">
       <option disabled selected >Member Role?</option>
       <option value="0">Admin</option>
       <option value="1">Manager</option>
       <option value="2">Moderator</option>
       <option value="3">Customer</option>
   </select>
   <input type="number" id="edituserRole" style="display: none;">
   <input type="number" id="user-id" style="display: none;">
</form>
    <button class="button-sec" id="save-edit">Update</button>
    <button class="button-primary cancel">Cancel</button>
  </div>
</div>




</body>

   
<?php echo $script?>
</html>