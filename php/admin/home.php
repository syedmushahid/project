<?php
include "config.php";
include "Modules/GetTotalNumbers.php";
?>


<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Admin | <?php echo $title ?></title>
    <?php echo $links ?>
</head>

<body>
    <div class="col-12">
        <div class="col-lg-2 col-md-3 col-xs-7">
            <?php include "menu.php" ?>
        </div>
     <div class="col-md-9 col-lg-10" >
     <?php include "adminheader.php"?>



<div class="col-12 mid-boxes" >

<div class="col-md-2 col-xs-5 top-result box-bg"><a href="users.php">Workers<br><br><?php echo totalRows("User where User_Role='4'") ?></a></div>
<div class="col-md-2 col-xs-5 top-result box-bg"><a href="users.php">Customers<br><br><?php echo totalRows("User where User_Role='3'") ?></a></div>
<div class="col-md-2 col-xs-5 top-result box-bg"><a href="Services.php">Services<br><br><?php echo totalRows("services") ?></a></div>
<div class="col-md-2 col-xs-5 top-result box-bg"> <a href="categories.php">Catagories<br><br><?php echo totalRows("category") ?></a></div>
</div>
<div class="col-12 mid-boxes">

<?php
        try {
        $query = "select * from category";
        $run = mysqli_query($connection, $query);
        if(mysqli_num_rows($run) > 0) {  
            $categoryName= array();
            $Post = array();
        while($row=mysqli_fetch_assoc($run))
        {
            $Post[] = $row["Total_Posts"];
            $categoryName[] = $row["Category_Name"];
        }
        unset($run);
        }
        else {
            echo "No Records Found";
        }
    } catch(PDOException $e) {
        die("ERROR: Could not be able to execute $sql. " . $e->getMessage());
    }

    try {
        $query = "select * from city";
        $run = mysqli_query($connection, $query);
        if(mysqli_num_rows($run) > 0) {
            $cityName = array();
            $user = array();
        while($row=mysqli_fetch_assoc($run))
        {
            $cityName[] = $row["City_Name"];
            $user[] = $row["Total_Users"];
        }
        unset($run);
        }
        else {
            echo "No Records Found";
        }
    } catch(PDOException $e) {
        die("ERROR: Could not be able to execute $sql. " . $e->getMessage());
    }
    ?>

  
<div class="col-md-5 col-xs-11 box-bg">
    <h2 class="box-top-h">Top Catogories</h2>
    <canvas id="categoryChart" width="700"></canvas>
</div>

<div class="col-md-5 col-xs-11 box-bg">
    <h2 class="box-top-h">Top Cities</h2>
    <canvas id="cityChart" width="700"></canvas>
</div>


    
<div class="col-xs-11 box-bg" style="margin-left: auto;margin-right: auto;">

<table>
  <caption>Recent Services</caption>
  <thead>
    <tr>
    <th scope="col">ID</th>
      <th scope="col">Title</th>
      <th scope="col">Category</th>
      <th scope="col">User Name</th>
    </tr>
  </thead>
  <tbody>
      <?php
        $query = "SELECT services.Service_Id AS Service_Id,
        CONCAT(user.First_Name,' ' ,user.Last_Name) AS Name,
        category.category_Name AS Category_Name, services.Title AS Title FROM services INNER JOIN
         category ON services.Category_Id=category.Category_Id INNER JOIN user 
         ON services.Worker_Id=user.id ORDER BY services.Service_Id DESC LIMIT 10";
        $run = mysqli_query($connection, $query);
        if(mysqli_num_rows($run) > 0) {  

          $rownum=0;
          $class;
        while($row=mysqli_fetch_assoc($run))
        {
            ?>
            <tr>
      <td data-label="ID"><?php echo$row['Service_Id']?></td>
      <td data-label="Title"><a target="_blank" href=<?php echo $hostname."post.php?id=".$row['Service_Id'];?>><?php echo$row['Title']?></a></td>
      <td data-label="Category"><?php echo$row['Category_Name']?></td>
      <td data-label="Name"><?php echo$row['Name']?></td>

    </tr>
       <?php $rownum++; } }?>
  </tbody>
</table>
</div>

     </div>
    
    </div>
       
</body>

<?php echo $script?>
<script>
    //---------------Category------------------------
    // setup block
    const post = <?php echo json_encode($Post);?>;
    const categoryName = <?php echo json_encode($categoryName);?>;
    const data = {
    labels: categoryName,
        datasets: [{
            label: 'Total Post: ',
            data: post,
            backgroundColor: [
                'rgba(255, 99, 132, 0.2)',
                'rgba(54, 162, 235, 0.2)',
                'rgba(255, 206, 86, 0.2)',
                'rgba(75, 192, 192, 0.2)',
                'rgba(153, 102, 255, 0.2)',
                'rgba(255, 159, 64, 0.2)'
            ],
            borderColor: [
                'rgba(255, 99, 132, 1)',
                'rgba(54, 162, 235, 1)',
                'rgba(255, 206, 86, 1)',
                'rgba(75, 192, 192, 1)',
                'rgba(153, 102, 255, 1)',
                'rgba(255, 159, 64, 1)'
            ],
            borderWidth: 1
        }]
    };
    //Config Block
    const config = {
        type: 'line',
    data,
    options: {
        
        scales: {
            y: {
                beginAtZero: true
            }
        }
    }
    }

    // render Category Block
    const categoryChart = new Chart(
        document.getElementById('categoryChart'),
        config
    );

    //---------------Cities------------------------
    
    //setup block
    const city = <?php echo json_encode($cityName);?>;
    const user = <?php echo json_encode($user);?>;
    const data2 = {
    labels: city,
        datasets: [{
            label: 'Total Users: ',
            data: user,
            backgroundColor: [
                'rgba(255, 99, 132, 0.2)',
                'rgba(54, 162, 235, 0.2)',
                'rgba(255, 206, 86, 0.2)',
                'rgba(75, 192, 192, 0.2)',
                'rgba(153, 102, 255, 0.2)',
                'rgba(255, 159, 64, 0.2)'
            ],
            borderColor: [
                'rgba(255, 99, 132, 1)',
                'rgba(54, 162, 235, 1)',
                'rgba(255, 206, 86, 1)',
                'rgba(75, 192, 192, 1)',
                'rgba(153, 102, 255, 1)',
                'rgba(255, 159, 64, 1)'
            ],
            borderWidth: 1
        }]
    };


    //Config Block
    const config2 = {
        type: 'line',
    data: data2,
    options: {
        scales: {
            y: {
                beginAtZero: true
            }
        }
    }
    }
    // render city Block
    const cityChart = new Chart(
        document.getElementById('cityChart'),
        config2
    );
</script>


</html>