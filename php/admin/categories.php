<?php
include "config.php";
include "Modules/GetTotalnumbers.php";
?>
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title> Categories | <?php echo $title ?></title>
  <?php echo $links  ?>
</head>

<body>
  <div class="col-12">
    <div class="col-lg-2 col-md-3 col-xs-7">
      <?php include "menu.php" ?>
    </div>
    <div class="col-md-9 col-lg-10">

      <?php include "adminheader.php" ?>


      <div class="col-12 mid-boxes">

        <div id="popupBtn" class="col-md-2 col-sm-5 top-result  mousePointer"><a><i style="font-size: 30px;" class="fas fa-plus"></i><br><br>Category </a>

        </div>
        <div class="col-md-5 searchBox">
          <input id="category-query" class="input-field"  type="text"><button id="search-category">Search</button>
        </div>
      </div>

      <div class="col-sm-11 box-bg" id="categories-table" style="margin-left: auto;margin-right: auto;">
        <?php include "../Database/LoadCategories.php"; ?>
      </div>
    </div>

    <div id="popupbg" class="modal">
      <div class="modal-content">
        <span class="close-popup">&times;</span>
        <h2 class="box-top-h">Add Category</h2>
        <form id="add-category">
          <input type="text" name="category" id="addingcategory" class="input-field" placeholder="Enter New Category" />
        </form>
        <button id="addcategory" class="button-sec">Add</button>
        <button class="cancel button-primary">Cancel</button>
      </div>
    </div>


    <div id="edit-category-modal" class="modal">
      <div class="modal-content">
        <span class="close-popup">&times;</span>
        <h2 class="box-top-h">Update Category</h2>
        <form id="update-category">
          <input type="text" name="category" id="editcategory" class="input-field" placeholder="Enter New Category" />
          <input type="number" id="category-id" style="display: none;">
        </form>
        <button id="save-edit" class="button-sec">Save</button>
        <button class="cancel button-primary">Cancel</button>
      </div>
    </div>
</body>
<?php echo $script ?>

</html>