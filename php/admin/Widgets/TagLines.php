<?php
include "../../config.php";
$sql = "SELECT * from tagline limit 1";
$result = mysqli_query($connection, $sql);
$heading = "";
$para = "";
$image = "";
if (mysqli_num_rows($result) > 0) {
    $row = mysqli_fetch_assoc($result);
    $heading = $row['Heading'];
    $para = $row['paragraph'];
    $image = $row['image'];
}

?>

<div style="align-items:flex-start" class="col-12" id="tagLine">
    <h2 class="box-top-h">Top Navbar Menu</h2>
    <form class="col-md-6" id="add-taglines">
        <lable><b>Heading</b></lable>
        <input type="text" value="<?php echo $heading; ?>" name="tag-heading" id="tag-heading" class="input-field" />
        <lable><b>Paragraph</b></lable>
        <textarea rows="6" name="tag-paragraph" id="tag-paragraph" class="input-field"><?php echo $para;?></textarea>


        <input type="file" name="tagimage" id="tagimage" />
        <br>
        <br>
    </form>
    <div class="col-md-6">

        <img style="width:90%;margin:auto" src="<?php echo "../../Uploads/".$image ?>" alt="">
    </div>

    <button id="addTagLine" class="button-sec">Update</button>
    <button id="cancelTagLine" class="cancel button-primary">Cancel</button>
    <br>
    <br>
</div>