<div id="homeCat">
    <h2 class="box-top-h">Home Page Categories</h2>
    <form id="add-home-cat-form">
        <input type="text" name="icon" id="caticon" class="input-field" placeholder="Enter Icon" />
        <input type="text" name="title" id="cattitle" class="input-field" placeholder="Enter Title" />
        <input type="text" name="link" id="catlink" class="input-field" placeholder="Enter Link" />
    </form>
    <button id="addHomeCat" class="button-sec">Add</button>
    <button id="cancelhomecat" class="cancel button-primary">Cancel</button>
    <br>
    <br>
    <div id="home-cat-list">
        <?php include "../../Database/widgets/LoadHomepageCat.php" ?>
    </div>
</div>



<div id="edit-homeCat-modal" class="modal">
    <div class="modal-content">
        <span class="close-popup">&times;</span>
        <h2 class="box-top-h">Update Page Link</h2>
        <form id="add-Page-Link-form">
            <input type="text" name="link" id="update-cat-icon" class="input-field" placeholder="Enter Icon" />
            <input type="text" name="title" id="update-cat-title" class="input-field" placeholder="Enter Title" />
            <input type="text" name="link" id="update-cat-link" class="input-field" placeholder="Enter Link" />
            <input type="number" id="catid" style="display: none;" />
        </form>
        <button id="updateHomeCat" class="button-sec">Update</button>
        <button class="cancel button-primary">Cancel</button>
    </div>
</div>