<?php
include "../../config.php";

$leftfile = fopen("../../footerleft.txt", "r") or die("Unable to open file!");
$left="";
if(filesize("../../footerleft.txt") > 0){
    $left=fread($leftfile,filesize("../../footerleft.txt"));;
}

$rightfile = fopen("../../footerright.txt", "r") or die("Unable to open file!");
$right="";
if(filesize("../../footerright.txt") > 0){
    $right=fread($rightfile,filesize("../../footerright.txt"));;
}

?>

<div style="align-items:flex-start" class="col-6">
    <h2 class="box-top-h">Footer Left</h2>
    <form class="col-12" id="footer-left">
        <textarea rows="12" name="left-footer-content" id="left-footer-content"  class="input-field"><?php echo $left;?></textarea>
        <br>
        <br>
    </form>
    <button id="update-footer-left" class="button-sec">Update</button>
    <button id="cancel-footer-left" class="cancel button-primary">Cancel</button>
</div>
<div style="align-items:flex-start" class="col-6">
    <h2 class="box-top-h">Footer Right</h2>
    <form class="col-12" id="footer-right">
        <textarea rows="12" name="right-footer-content" id="right-footer-content"  class="input-field"><?php echo $right;?></textarea>
        <br>
        <br>
    </form>
    <button id="update-footer-right" class="button-sec">Update</button>
    <button id="cancel-footer-right" class="cancel button-primary">Cancel</button>
    <br>
    <br>
</div>
<?php fclose($leftfile);?>
<?php fclose($rightfile);?>


<div id="topnav">
    <h2 class="box-top-h">Footer Menu</h2>
    <form id = "add-Page-Link-form">
    <input type="text" name="title" id="title" class="input-field" placeholder="Enter Page Title" />
      <input type="text" name="link" id="link" class="input-field" placeholder="Enter Page Link" />
    </form>
    <button id="addFooterMenu" class="button-sec">Add</button>
    <button id="cancelFooterMenu" class="cancel button-primary">Cancel</button>
  <br>
  <br>
  <div id="footerlinkList">
<?php include "../../Database/widgets/LoadFooterMenu.php"?>
</div>
</div>

<br>
<br>

<div id="edit-Footer-modal" class="modal">
  <div class="modal-content">
    <span class="close-popup">&times;</span>
    <h2 class="box-top-h">Update Page Link</h2>
    <form id = "add-Page-Link-form">
    <input type="text" name="title" id="updatetitle" class="input-field" placeholder="Enter Page Title" />
      <input type="text" name="link" id="updatelink" class="input-field" placeholder="Enter Page Link" />
   <input type="number" id="linkid" style="display: none;"/>
    </form>
    <button id="updateFooterMenu" class="button-sec">Update</button>
    <button class="cancel button-primary">Cancel</button>
  </div>
</div>