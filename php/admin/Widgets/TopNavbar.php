
<div id="topnav">
    <h2 class="box-top-h">Top Navbar Menu</h2>
    <form id = "add-Page-Link-form">
    <input type="text" name="title" id="title" class="input-field" placeholder="Enter Page Title" />
      <input type="text" name="link" id="link" class="input-field" placeholder="Enter Page Link" />
    </form>
    <button id="addPageLink" class="button-sec">Add</button>
    <button id="cancellink" class="cancel button-primary">Cancel</button>
  <br>
  <br>
  <div id="toplinkList">
<?php include "../../Database/widgets/LoadTopLinks.php"?>
</div>
</div>



<div id="edit-topNav-modal" class="modal">
  <div class="modal-content">
    <span class="close-popup">&times;</span>
    <h2 class="box-top-h">Update Page Link</h2>
    <form id = "add-Page-Link-form">
    <input type="text" name="title" id="updatetitle" class="input-field" placeholder="Enter Page Title" />
      <input type="text" name="link" id="updatelink" class="input-field" placeholder="Enter Page Link" />
   <input type="number" id="linkid" style="display: none;"/>
    </form>
    <button id="updatePageLink" class="button-sec">Update</button>
    <button class="cancel button-primary">Cancel</button>
  </div>
</div>