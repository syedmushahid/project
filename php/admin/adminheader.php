<?php include "config.php";
$pagetitle = $title;
$heading = basename($_SERVER["PHP_SELF"]);
switch ($heading) {

    case "home.php": {
            $pagetitle = "Admin Dashboard";
        }
    case "admin": {
            $pagetitle = "Admin Dashboard";
        }
        break;
    case "users.php": {
            $pagetitle = "Users";
        }
        break;
    case "categories.php": {
            $pagetitle = "Catagories";
        }
        break;
    case "cities.php": {
            $pagetitle = "Cities";
        }
        break;
    case "widget.php": {
            $pagetitle = "Widget";
        }
        break;
    case "services.php": {
            $pagetitle = "Services";
        }
        break;
    case "teammembers.php": {
            $pagetitle = "Team Members";
        }
        break;
    case "visitors.php": {
            $pagetitle = "Web Visitors";
        }
}

if (session_status() === PHP_SESSION_NONE) {
    session_start();
}
if (!isset($_SESSION['user_id']) || $_SESSION['user_role'] == '3' || $_SESSION['user_role'] == '4') {
    header("location:{$hostname}");
}


?>

<div class="col-12 adminheader " style="justify-content:space-around">
    <div class="col-8">
    <h2> <span style="float:left;"><i class="fas fa-bars" id="open-admin-menu"></i>&nbsp;&nbsp;
            </span>  <?php echo $pagetitle ?></h2>
    </div>
    <div class=" dropdown-wrapper">
        <div id="topusername">
                <i class="fas fa-user"></i>&nbsp;  <?php echo $_SESSION['first_name'] ?> &nbsp; <i style="float: right; margin-right:5px" class="fas fa-caret-down"></i>
          

        </div>


        <div id="dropdown">
        <a href="<?php echo $hostname;?>">
        <div class="topdropdown"><i class="fas fa-home"></i> Home</div>

    </a>
            <a href="../profile.php">
                <div class="topdropdown"><i class="fas fa-user"></i> Profile</div>
            </a>
            <?php if ($_SESSION['user_role'] == 0 || $_SESSION['user_role'] == 1 || $_SESSION['user_role'] == 2) {
            ?> <a href="home.php">
                    <div class="topdropdown"><i class="fas fa-th-large"></i> Admin Dashboard</div>
                </a>
            <?php } ?>
            <a href="../profile-settings.php">
                <div class="topdropdown"><i class="fas fa-cog"></i> Settings</div>
            </a>
            <a href="../Modules/logout.php">
                <div class="topdropdown"><i class="fas fa-sign-out-alt"></i> Logout</div>
        </div></a>

    </div>

</div>