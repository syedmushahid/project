$(document).ready(function () {
  // menu active tab
  let url = window.location.href.split('?')[0];
console.log(url);
  //////////Message BOxes//////////
/////////Responsive Menue Toogle///////////
$(document).on("click","#open-admin-menu",function(){
 
$(".menu").fadeIn(300);
$("#close-admin-menu").show();
});
$(document).on("click","#close-admin-menu",function(){

  $(".menu").fadeOut(300);
  
  });


  ///////////////top drop down menue///////
  $("#topusername").click(function () {
    $("#dropdown").toggle();
  });
  $(window).click(function (event) {
    if (event.target.id != "topusername" && event.target.id != "dropdown") {
      $("#dropdown").hide();
    }
  });

  function addWarningMessage(message) {
    let content = `<div  class="message-box warning-message">
 <div><i class="fas fa-exclamation-triangle"></i>&nbsp;&nbsp;&nbsp;
 <span>
 ${message}
 </span>
</div>
</div>
`;
    $(".message-box").remove();
    $("body").prepend(content);
    $(".warning-message").slideDown(200).delay(3000).fadeOut();
  }

  function addErrorMessage(message) {
    let content = `<div class="message-box error-message">
  <div><i class="fas fa-skull-crossbones"></i>
  &nbsp;&nbsp;&nbsp;<span>${message}</span>
 </div>
 </div>`;
    $(".message-box").remove();
    $("body").prepend(content);
    $(".error-message").slideDown(200).delay(3000).fadeOut();
  }
  function addSuccessMessage(message) {
    let content = `<div  class="message-box success-message">
  <div><i class="fas fa-check-circle"></i>
  &nbsp;&nbsp;&nbsp;<span>${message}</span>
 </div>
 </div>`;
    $(".message-box").remove();
    $("body").prepend(content);
    $(".success-message").slideDown(200).delay(3000).fadeOut();
  }

  ///////Cobnfirmation box/////////
  function Confirmation(title, message, yes, cancel, succssAction) {
    var content =
      "<div id='confirm' class='confirm'>" +
      "<div class='dialog'><div class='confirm-title'>" +
      " <h3> " +
      title +
      " </h3> " +
      "  <span id='close-confirm'>&times;</span>" +
      "</div>" +
      "<div class='confirm-message'>" +
      " <p> " +
      message +
      " </p> " +
      "</div>" +
      "<div class='confirm-options'>" +
      " <button class='button-sec confirm-btn confirmAction'>" +
      yes +
      "</button> " +
      " <button class='button-primary cancel-btn cancelAction'>" +
      cancel +
      "</button> " +
      "</div>";
    "</div>" + "</div>";
    $("body").prepend(content);

    let confirmBox = document.getElementById("confirm");
    window.onclick = function (event) {
      if (event.target == confirmBox) {
        $("#confirm").fadeOut(400, function () {
          $(this).remove();
        });
      }
    };
    $(".confirmAction").click(function () {
      succssAction();
      $(this)
        .parents(".confirm")
        .fadeOut(400, function () {
          $(this).remove();
        });
    });
    $(".cancelAction, #close-confirm").click(function () {
      $(this)
        .parents(".confirm")
        .fadeOut(400, function () {
          $(this).remove();
        });
    });
  }
  /////////Load Data////////////
  function loadData(targetUrl, targetDiv) {
    $.ajax({
      url: targetUrl,
      type: "POST",
      success: function (data) {
        $(targetDiv).html(data);
      },
      error: function () {
        addErrorMessage("Error 404");
      },
    });
  }

  // Get all buttons with class="btn" inside the container
  var btns = document.getElementsByClassName("menubtn");
  // Loop through the buttons and add the active class to the current/clicked button
  for (var i = 0; i < btns.length; i++) {
    if (btns[i].href == url) {
      btns[i].classList.add("activetab");
    }
  }

  /////////Add data Modal///////////
  $("#popupBtn").click(function () {
    $("#popupbg").fadeIn(300);
  });
  $(document).on("click", ".close-popup", function () {
    $(".modal").fadeOut(400);
  });
  $(document).on("click", ".modal", function (e) {
    if (e.target !== this) return;
    $(".modal").fadeOut(400);
  });
  $(document).on("click", ".cancel", function () {
    $(".modal").fadeOut(400);
  });

  //////Update-Edit Category Modal/////

  $(document).on("click", ".category-edit", function () {
    let categoryName = $(this).data("name");
    let categoryID = $(this).data("id");
    $("#edit-category-modal").show();
    $("#editcategory").val(categoryName);
    $("#category-id").val(categoryID);
  });

  $(document).on("click", "#save-edit", function () {
    let categoryName = $("#editcategory").val();
    let categoryID = $("#category-id").val();
    if (categoryName != "") {
      $.ajax({
        url: "../Database/UpdateCategory.php",
        type: "POST",
        data: { id: categoryID, name: categoryName },
        success: function (data) {
          if (data == 0) {
            addErrorMessage(" An Error Occured Kindly Try Again");
          }
          if (data == 2) {
            addWarningMessage(" Category Cant Be Empty");
          }
          if (data == 1) {
            addSuccessMessage("Category Updated Successfuly");
            loadData("../Database/LoadCategories.php", "#categories-table");
            $(".modal").fadeOut(400);
          }
        },
        error: function () {
          addErrorMessage("Error 404");
        },
      });
    } else {
      addWarningMessage(" Category Cant Be Empty");
    }
  });

  //////Update-Edit User Role Modal/////

  $(document).on("click", ".userRole-edit", function () {
    let userRole = $(this).data("role");
    let userID = $(this).data("id");
    $("#edit-userRole-modal").show();
    $("#userRole").val(userRole);
    $("#user-id").val(userID);
  });

  $(document).on("click", "#save-edit", function () {
    let userRole = $("#userRole").val();
    let userID = $("#user-id").val();
    $.ajax({
      url: "../Database/UpdateUserRole.php",
      type: "POST",
      data: { userID: userID, userRole: userRole },
      success: function (data) {
        console.log(data);
        if (data == 0) {
          addErrorMessage(" An Error Occured Kindly Try Again");
        }
        if (data == 2) {
          addWarningMessage(" User Role Cant Be Empty");
        }
        if (data == 1) {
          addSuccessMessage("User Role Updated Successfuly");
          loadData("../Database/LoadTeamMember.php", "#table-data");
          $(".modal").fadeOut(400);
        }
      },
      error: function () {
        addErrorMessage("Error 404");
      },
    });
  });


  //////Update User Ban/Unban/////
  $(document).on("click", ".update_ban", function () {
    let id = $(this).data("id");
    let ban = $(this).data("ban");

    $.ajax({
      url: "../Database/BanUser.php?id="+id+"&ban="+ban,
      type: "POST",
      data: { id: id, ban: ban },
      contentType: false,
      processData: false,
      success: function (data) {
        if (data == 1) {
          if(ban == "unban") {
            addSuccessMessage("User Banned Successfully");
          } else {
            addSuccessMessage("User Unbanned Successfully");
          }
          loadData("../Database/LoadUsers.php", "#user-table");
          
        }
        if (data == 2) {
          addWarningMessage("An Error Occured Kindly Try Again");
        }
      },
      error: function () {
        addErrorMessage("Error 404");
      },
    });
  });

  //////Update-Edit City Modal/////

  $(document).on("click", ".city-edit", function () {
    let cityName = $(this).data("name");
    let cityID = $(this).data("id");
    console.log(this);
    $("#edit-city-modal").show();
    $("#editcity").val(cityName);
    $("#city-id").val(cityID);
  });

  $(document).on("click", "#save-edit", function () {
    let cityName = $("#editcity").val();
    let cityID = $("#city-id").val();
    if (cityName != "") {
      $.ajax({
        url: "../Database/UpdateCity.php",
        type: "POST",
        data: { id: cityID, name: cityName },
        success: function (data) {
          if (data == 0) {
            addErrorMessage(" An Error Occured Kindly Try Again");
          }
          if (data == 2) {
            addWarningMessage(" City Cant Be Empty");
          }
          if (data == 1) {
            addSuccessMessage("City Updated Successfuly");
            loadData("../Database/LoadCity.php", "#citytable");
            $(".modal").fadeOut(400);
          }
        },
        error: function () {
          addErrorMessage("Error 404");
        },
      });
    } else {
      addWarningMessage(" Category Cant Be Empty");
    }
  });

  /////////// To Delete City//////////////
  $(document).on("click", ".city-delete", function () {
    let targetElement = this;
    let cityID = $(this).data("id");
    let cityName = $(this).data("name");
    function succssAction() {
      $.ajax({
        url: "../Database/DeleteCity.php",
        type: "POST",
        data: { id: cityID },
        success: function (data) {
          if (data == 2) {
            addErrorMessage(" An Error Occured Kindly Try Again");
          }
          if (data == 1) {
            addSuccessMessage(" City Deleted Successfully");
            $(targetElement).closest("tr").fadeOut();
          }
        },
        error: function () {
          addErrorMessage("error 404");
        },
      });
    }
    Confirmation(
      "Delete City",
      "Are you sure you want to Delete " + cityName + " ?",
      "Yes",
      "Cancel",
      succssAction
    );
  });

  ////////////Add Categories/////////////
  $("#addcategory").on("click", function () {
    let category = $("#addingcategory").val();
    if (category != "") {
      $.ajax({
        url: "../Database/AddCategory.php",
        type: "POST",
        data: { category: category },
        success: function (data) {
          if (data == 0) {
            addWarningMessage("Kindly Enter Your Category.");
          }
          if (data == 2) {
            addErrorMessage(" An Error Occured Kindly Try Again");
          }
          if (data == 3) {
            addErrorMessage(" Category Already Exists.");
          }
          if (data == 1) {
            addSuccessMessage("Category Added Successfuly");
            document.getElementById("add-category").reset();
            loadData("../Database/LoadCategories.php", "#categories-table");
          }
        },
        error: function () {
          addErrorMessage("Error 404");
        },
      });
    } else {
      addWarningMessage("Kindly Enter Your Category.");
    }
  });

  /////////// To Delete Category//////////////
  $(document).on("click", ".category-delete", function () {
    let targetElement = this;
    let categoryID = $(this).data("id");
    let categoryName = $(this).data("name");
    alert(categoryID);
    alert(categoryName);
    function succssAction() {
      $.ajax({
        url: "../Database/DeleteCategory.php",
        type: "POST",
        data: { id: categoryID },
        success: function (data) {
          if (data == 2) {
            addErrorMessage(" An Error Occured Kindly Try Again");
          }
          if (data == 1) {
            addSuccessMessage(" Category Deleted Successfully");
            $(targetElement).closest("tr").fadeOut();
          }
        },
        error: function () {
          addErrorMessage("error 404");
        },
      });
    }
    Confirmation(
      "Delete Category",
      "Are you sure you want to Delete " + categoryName + " ?",
      "Yes",
      "Cancel",
      succssAction
    );
  });

  /////////////Search Category//////////
  $("#search-category").on("click", function () {
    let query = $("#category-query").val();
    loadData("../Database/LoadCategories.php?search="+query, "#categories-table");
  });
   /////////////Search City//////////
   $("#search-city").on("click", function () {
    let query = $("#city-query").val();
    loadData("../Database/LoadCity.php?search="+query, "#citytable");
  });
   /////////////Search TeamMember//////////
   $("#search-member").on("click", function () {
    let query = $("#member-query").val();
    loadData("../Database/LoadTeamMember.php?search="+query, "#table-data");
  });
     /////////////Search User//////////
     $("#search-user").on("click", function () {
      let query = $("#user-query").val();
      loadData("../Database/LoadUsers.php?search="+query, "#user-table");
    });
     /////////////Search Services//////////
     $("#search-services").on("click", function () {
      let query = $("#services-query").val();
      loadData("../Database/Loadservices.php?search="+query, "#services-table");
    });
  
  /////////// To Delete Services//////////////
  $(document).on("click", ".service-delete", function () {
    let targetElement = this;
    let serviceID = $(this).data("id");
    let title =$("#posttitle"+serviceID).val();
    let email =$("#email"+serviceID).val();
    function succssAction() {
      $.ajax({
        url: "../Database/DeleteService.php",
        type: "POST",
        data: { id: serviceID,email:email,title:title },
        success: function (data) {
          console.log(data);
          if (data == 2) {
            addErrorMessage(" An Error Occured Kindly Try Again");
          }
          if (data == 1) {
            addSuccessMessage(" Service Deleted Successfully");
            $(targetElement).closest("tr").fadeOut();
            
          }
        },
        error: function () {
          addErrorMessage("error 404");
        },
      });
    }
    Confirmation(
      "Delete Service",
      "Are you sure you want to Delete?<br>Title: '<b>" + title + "</b>'",
      "Yes",
      "Cancel",
      succssAction
    );
  });

  ///////////Add Team Members////////////
  $("#addMember").on("click", function () {
    let form = new FormData(document.getElementById("AddMember"));
    $.ajax({
      url: "../Database/AddMember.php",
      type: "POST",
      data: form,
      contentType: false,
      processData: false,
      success: function (data) {
        if (data == 0) {
          addWarningMessage("Kindly Fill All Blanks");
        }
        if (data == 4) {
          addWarningMessage("Password Does Not Match");
        }
        if (data == 2) {
          addErrorMessage(" An Error Occured Kindly Try Again");
        }
        if (data == 1) {
          addSuccessMessage("Member added sucessfuly");
          document.getElementById("AddMember").reset();
          loadData("../Database/LoadCategories.php", "#user-table");

          // $.ajax({
          //   url : "../Database/LoadTeamMember.php",
          //   type : "POST",
          //   success : function(data){
          //     $("#table-data").html(data);
          //   }
          // });
        }
        if (data == 3) {
          addErrorMessage("Member Already Exists");
        }
      },
      error: function () {
        addErrorMessage("error 404");
      },
    });
  });

  ////////////Add Cities/////////////
  $("#addcity").on("click", function () {
    let city = $("#add-City").val();
    if (city != "") {
      $.ajax({
        url: "../Database/AddCities.php",
        type: "POST",
        data: { city: city },
        success: function (data) {
          console.log(data);
          if (data == 0) {
            addWarningMessage("Kindly Enter the city");
          }
          if (data == 2) {
            addErrorMessage(" An Error Occured Kindly Try Again");
          }
          if (data == 3) {
            addErrorMessage(" City Already Exists.");
          }
          if (data == 1) {
            addSuccessMessage("City Added Successfuly");
            document.getElementById("add-city").reset();
            loadData("../Database/LoadCity.php","#citytable")
          }
        },
        error: function () {
          addErrorMessage("Error 404");
        },
      });
    } else {
      addWarningMessage("Kindly Enter the city");
    }
  });

  //// // Load TeamMembers Table Data//////////
  $(document).on("change", "#get-user-table", function () {
    var userrole = $(this).val();

    if (userrole == "") {
      $("#table-data").html("");
    } else {
      $.ajax({
        url: "../Database/LoadTeamMember.php",
        type: "POST",
        data: { Role: userrole },
        success: function (data) {
          $("#table-data").html(data);
        },
      });
    }
  });

  //// // Load User Data//////////
  $(document).on("change", "#get-user-table", function () {
    var userrole = $(this).val();

    if (userrole == "") {
      $("#table-data").html("");
    } else {
      $.ajax({
        url: "../Database/LoadTeamMember.php",
        type: "POST",
        data: { Role: userrole },
        success: function (data) {
          $("#table-data").html(data);
        },
      });
    }
  });

  ////////////Widgets Page Starts////////////

  $("#selectWidget").on("change", function () {
    let targetfile = "nofile";
    let value = $("#selectWidget").val();

    switch (value) {
      case "1": {
        targetfile = "Widgets/TopNavbar.php";

        break;
      }
      case "2": {
        targetfile = "Widgets/Logo.php";

        break;
      }
      case "3": {
        targetfile = "Widgets/MainNavbar.php";

        break;
      }
      case "4": {
        targetfile = "Widgets/TagLines.php";

        break;
      }
      case "5": {
        targetfile = "Widgets/Categories.php";

        break;
      }
      case "6": {
        targetfile = "Widgets/Html-Text.php";

        break;
      }
      case "7": {
        targetfile = "Widgets/Footer.php";

        break;
      }
    }

    loadData(targetfile, "#widget-WorkArea");
  });

  ///Top Navbar///////

  $(document).on("click", "#cancellink", function () {
    document.getElementById("add-Page-Link-form").reset();
  });
  $(document).on("click", "#addPageLink", function () {
    let link = $("#link").val();
    let title = $("#title").val();

    if (link != "" && title != "") {
      $.ajax({
        url: "../Database/widgets/AddTopLinks.php",
        type: "POST",
        data: { link: link, title: title },
        success: function (data) {
          if (data == 0) {
            addWarningMessage("Kindly Fill All Blanks");
          }
          if (data == 1) {
            document.getElementById("add-Page-Link-form").reset();
            addSuccessMessage("Page Added Successfuly");
            loadData("../Database/widgets/LoadTopLinks.php", "#toplinkList");
          }
          if (data == 2) {
            addErrorMessage(" An Error Occured Kindly Try Again");
          }
        },
        error: function () {
          addErrorMessage("Error 404");
        },
      });
    } else {
      addWarningMessage("Kindly Fill ALl Blanks");
    }
  });

  //////Edit links/////
  $(document).on("click", ".pageLink-edit", function () {
    let linkid = $(this).data("linkid");
    let link = $(this).data("link");
    let title = $(this).data("title");
    $("#updatetitle").val(title);
    $("#updatelink").val(link);
    $("#linkid").val(linkid);
    $("#edit-topNav-modal").fadeIn(400);
  });
  $(document).on("click", "#updatePageLink", function () {
    let title = $("#updatetitle").val();
    let link = $("#updatelink").val();
    let linkid = $("#linkid").val();
    if (title != "" && link != "" && linkid != "") {
      alert(link);
      $.ajax({
        url: "../Database/widgets/UpdateLinks.php",
        type: "POST",
        data: { linkid: linkid, title: title, link: link },
        success: function (data) {
          alert(data);
          if (data == 0) {
            addErrorMessage(" An Error Occured Kindly Try Again");
          }
          if (data == 2) {
            addWarningMessage(" Kindly Fill All Fileds");
          }
          if (data == 1) {
            addSuccessMessage("Page Link Updated Successfuly");
            loadData("../Database/widgets/LoadTopLinks.php", "#toplinkList");

            $(".modal").fadeOut(400);
          }
        },
        error: function () {
          addErrorMessage("Error 404");
        },
      });
    } else {
      addWarningMessage(" Kindly Fill All Fileds");
    }
  });

  ///////Delete Top Page Links/////////////
  $(document).on("click", ".pageLink-delete", function () {
    let targetElement = this;
    let linkid = $(this).data("linkid");
    let title = $(this).data("title");

    function succssAction() {
      $.ajax({
        url: "../Database/widgets/deletelink.php",
        type: "POST",
        data: { linkid: linkid },
        success: function (data) {
          if (data == 2 || data == 3) {
            addErrorMessage(" An Error Occured Kindly Try Again");
          }
          if (data == 1) {
            addSuccessMessage(" Page Link Deleted Successfully");
            $(targetElement).closest("tr").fadeOut();
          }
        },
        error: function () {
          addErrorMessage("error 404");
        },
      });
    }
    Confirmation(
      "Delete Page Link",
      "Are you sure you want to Delete " + title + " ?",
      "Yes",
      "Cancel",
      succssAction
    );
  });

  /////////Upload new logo////////////

  $(document).on("change", "#newlogo", function () {
    let form = new FormData(document.getElementById("upload-logo"));
    $.ajax({
      url: "../Database/Widgets/Uploadlogo.php",
      type: "POST",
      data: form,
      contentType: false,
      processData: false,
      success: function (data) {
        if (data == 0) {
          $("#newlogo").val(null);
          addErrorMessage("Only Images Are Allowed To Upload");
        } else if (data == 1) {
          $("#newlogo").val(null);
          addErrorMessage("Error! Image Size Is Greater Than 2 MB");
        } else {
          $(".message-box").remove();
          $("#logopreview").html(data);
        }
      },
    });
  });

  $(document).on("click", "#savelogo", function () {
    $.ajax({
      url: "../Database/Widgets/savelogo.php",
      type: "POST",
      contentType: false,
      processData: false,
      success: function (data) {
        if (data == 1) {
          $("#logopreview").html(" ");
          addSuccessMessage("Logo Updated");
          loadData(
            "Widgets/Logo.php",
            "#widget-WorkArea"
          );
        }
        if (data == 0) {
          addErrorMessage("Error! can't update Logo");
        }
        if (data == 2) {
          addWarningMessage("No Image Selected");
        }
      },
      error: function () {
        addErrorMessage("error 404");
      },
    });
  });

  ////////MAin Navabr/////////////////////
  $(document).on("click", ".add-navbar-link ", function () {
    let category = $(this).data("id");
    let targetElement = this;
    if (category != "") {
      $.ajax({
        url: "../Database/Widgets/AddMainNavbar.php",
        type: "POST",
        data: { category: category },
        success: function (data) {
          console.log(data);
          if (data == 2) {
            addErrorMessage(" An Error Occured Kindly Try Again");
          }
          if (data == 1) {
            addSuccessMessage("New Link Added Successfuly");
            loadData(
              "../Database/widgets/LoadMainNavbarLinks.php",
              "#navlinks"
            );
            $(targetElement).closest("tr").fadeOut();
          }
        },
        error: function () {
          addErrorMessage("Error 404");
        },
      });
    } else {
      addWarningMessage(" Category Id Cant Be Empety");
    }
  });

  /////////To Remove Main Navbar Links/////////
  $(document).on("click", ".remove-link", function () {
    let targetElement = this;
    let categoryID = $(this).data("id");
    let categoryName = $(this).data("name");
    function succssAction() {
      $.ajax({
        url: "../Database/widgets/DeleteMainNavbarLink.php",
        type: "POST",
        data: { id: categoryID },
        success: function (data) {
          console.log(data);
          if (data == 2) {
            addErrorMessage(" An Error Occured Kindly Try Again");
          }
          if (data == 1) {
            addSuccessMessage(" Link Removed Successfully");
            $(targetElement).closest("span").fadeOut();
            loadData(
              "../Database/widgets/LoadAvailableLinks.php",
              "#availablelinks"
            );
          }
          if (data == 0) {
            addErrorMessage(" An Error Occured Kindly Try Again");
            $(targetElement).closest("span").fadeOut();
          }
        },
        error: function () {
          addErrorMessage("error 404");
        },
      });
    }
    Confirmation(
      "Remove Navbar Link",
      "Are you sure you want to Remove " + categoryName + " ?",
      "Yes",
      "Cancel",
      succssAction
    );
  });

  ////////////Tag Line Widget////////////

  $(document).on("click", "#cancelTagLine", function () {
    loadData(
      "Widgets/TagLines.php",
      "#widget-WorkArea"
    );
  });
  $(document).on("click", "#addTagLine", function () {
    let form = new FormData(document.getElementById("add-taglines"));

    $.ajax({
      url: "../Database/widgets/addTagLineData.php",
      type: "POST",
      data: form,
      contentType: false,
      processData: false,
      success: function (data) {
        console.log(data);
        if (data == "0") {
          addWarningMessage("File Format Not Supported");
        } else if (data == "1") {
          addWarningMessage("File Size cant be Greater than 2MB");
        } else if (data == "2") {
          addSuccessMessage("Data Added Sucessfully");
          loadData(
            "Widgets/TagLines.php",
            "#widget-WorkArea"
          );
        } else if (data == "3") {
          addSuccessMessage("Data Updated Sucessfully");
        } else {
          addErrorMessage("Error! Cant Update Data");
        }
      },
    });
  });


  ////////////Home Page Categories////////////////////

  $(document).on("click", "#cancelhomecat", function () {
    document.getElementById("add-home-cat-form").reset();
  });
  
  $(document).on("click", "#addHomeCat", function () {
    let link = $("#catlink").val();
    let title = $("#cattitle").val();
    let icon = $("#caticon").val();
    if (link != "" && title != "" && icon !="") {
      $.ajax({
        url: "../Database/widgets/AddHomeCat.php",
        type: "POST",
        data: { link: link, title: title ,icon:icon},
        success: function (data) {
          if (data == 0) {
            addWarningMessage("Kindly Fill All Blanks");
          }
          if (data == 1) {
            document.getElementById("add-Page-Link-form").reset();
            addSuccessMessage("Page Added Successfuly");
            loadData("../Database/widgets/LoadHomepageCat.php", "#home-cat-list");
          }
          if (data == 2) {
            addErrorMessage(" An Error Occured Kindly Try Again");
          }
        },
        error: function () {
          addErrorMessage("Error 404");
        },
      });
    } else {
      addWarningMessage("Kindly Fill ALl Blanks");
    }
  });

  //////Edit Home Categories/////
  $(document).on("click", ".home-cat-edit", function () {
    let catid = $(this).data("catid");
    let link = $(this).data("link");
    let title = $(this).data("title");
    let icon = $(this).data("icon");
   


    $("#update-cat-title").val(title);
    $("#update-cat-link").val(link);
    $("#update-cat-icon").val(icon);
    $("#catid").val(catid);
    $("#edit-homeCat-modal").fadeIn(400);
  });
  $(document).on("click", "#updateHomeCat", function () {
    let title = $("#update-cat-title").val();
    let link = $("#update-cat-link").val();
    let icon = $("#update-cat-icon").val();
    let catid = $("#catid").val();
    if (title != "" && link != "" && catid != "") {
      
      $.ajax({
        url: "../Database/widgets/UpdateHomeCat.php",
        type: "POST",
        data: { catid: catid, title: title, link: link ,icon:icon},
        success: function (data) {
          if (data == 0) {
            addErrorMessage(" An Error Occured Kindly Try Again");
          }
          if (data == 2) {
            addWarningMessage(" Kindly Fill All Fileds");
          }
          if (data == 1) {
            addSuccessMessage("Page Link Updated Successfuly");
            loadData("../Database/widgets/LoadHomepageCat.php", "#home-cat-list");
            $(".modal").fadeOut(400);
          }
        },
        error: function () {
          addErrorMessage("Error 404");
        },
      });
    } else {
      addWarningMessage(" Kindly Fill All Fileds");
    }
  });

  ///////Delete Top Page Links/////////////
  $(document).on("click", ".home-cat-delete", function () {
    let targetElement = this;
    let catid = $(this).data("catid");
    let title = $(this).data("title");


    function succssAction() {
      $.ajax({
        url: "../Database/widgets/deleteHomepageCat.php",
        type: "POST",
        data: { catid: catid },
        success: function (data) {
          
          if (data == 2 || data == 3) {
            addErrorMessage(" An Error Occured Kindly Try Again");
          }
          if (data == 1) {
            addSuccessMessage(" Page Link Deleted Successfully");
            $(targetElement).closest("tr").fadeOut();
          }
        },
        error: function () {
          addErrorMessage("error 404");
        },
      });
    }
    Confirmation(
      "Delete Home Page Category",
      "Are you sure you want to Delete " + title + " ?",
      "Yes",
      "Cancel",
      succssAction
    );
  });

  //////////////Html Text Content//////////////////
  
  $(document).on("click", "#cancel-html-content", function () {
    loadData(
      "Widgets/Html-Text.php",
      "#widget-WorkArea"
    );
  });
  $(document).on("click", "#update-html-content", function () {
    let form = new FormData(document.getElementById("add-taglines"));

    $.ajax({
      url: "../Database/widgets/writeintoFile.php",
      type: "POST",
      data: form,
      contentType: false,
      processData: false,
      success: function (data) {
        console.log(data);
      if (data == "1") {
          addSuccessMessage("Data Added Sucessfully");
          loadData(
            "Widgets/Html-Text.php",
            "#widget-WorkArea"
          );
        } else {
          addErrorMessage("Error! Cant Update Data");
        }
      },
    });
  });


////////////Footer Left///////////////////
  
 $(document).on("click", "#cancel-footer-left", function () {
  loadData(
    "Widgets/Footer.php",
    "#widget-WorkArea"
  );
});
$(document).on("click", "#update-footer-left", function () {
  let form = new FormData(document.getElementById("footer-left"));

  $.ajax({
    url: "../Database/widgets/writeLeftFooter.php",
    type: "POST",
    data: form,
    contentType: false,
    processData: false,
    success: function (data) {
      console.log(data);
    if (data == "1") {
        addSuccessMessage("Data Added Sucessfully");
        loadData(
          "Widgets/Footer.php",
          "#widget-WorkArea"
        );
      } else {
        addErrorMessage("Error! Cant Update Data");
      }
    },
  });
});


////////////Footer Right///////////////////
  
$(document).on("click", "#cancel-footer-right", function () {
  loadData(
    "Widgets/Footer.php",
    "#widget-WorkArea"
  );
});
$(document).on("click", "#update-footer-right", function () {
  let form = new FormData(document.getElementById("footer-right"));

  $.ajax({
    url: "../Database/widgets/writerightFooter.php",
    type: "POST",
    data: form,
    contentType: false,
    processData: false,
    success: function (data) {
      console.log(data);
    if (data == "1") {
        addSuccessMessage("Data Added Sucessfully");
        loadData(
          "Widgets/Footer.php",
          "#widget-WorkArea"
        );
      } else {
        addErrorMessage("Error! Cant Update Data");
      }
    },
  });
});


///////////////Footer Menu//////////////////

  $(document).on("click", "#cancelFooterMenu", function () {
    document.getElementById("add-Page-Link-form").reset();
  });
  $(document).on("click", "#addFooterMenu", function () {
    let link = $("#link").val();
    let title = $("#title").val();

    if (link != "" && title != "") {
      $.ajax({
        url: "../Database/widgets/AddFooterMenu.php",
        type: "POST",
        data: { link: link, title: title },
        success: function (data) {
          if (data == 0) {
            addWarningMessage("Kindly Fill All Blanks");
          }
          if (data == 1) {
            document.getElementById("add-Page-Link-form").reset();
            addSuccessMessage("Page Added Successfuly");
            loadData("../Database/widgets/LoadFooterMenu.php", "#footerlinkList");
          }
          if (data == 2) {
            addErrorMessage(" An Error Occured Kindly Try Again");
          }
        },
        error: function () {
          addErrorMessage("Error 404");
        },
      });
    } else {
      addWarningMessage("Kindly Fill ALl Blanks");
    }
  });

  //////Edit Footer Menu/////
  $(document).on("click", ".footerMenu-edit", function () {
    let linkid = $(this).data("linkid");
    let link = $(this).data("link");
    let title = $(this).data("title");
    $("#updatetitle").val(title);
    $("#updatelink").val(link);
    $("#linkid").val(linkid);
    $("#edit-Footer-modal").fadeIn(400);
  });
  $(document).on("click", "#updateFooterMenu", function () {
    let title = $("#updatetitle").val();
    let link = $("#updatelink").val();
    let linkid = $("#linkid").val();
    if (title != "" && link != "" && linkid != "") {
      $.ajax({
        url: "../Database/widgets/UpdateFooterMenu.php",
        type: "POST",
        data: { linkid: linkid, title: title, link: link },
        success: function (data) {

          if (data == 0) {
            addErrorMessage(" An Error Occured Kindly Try Again");
          }
          if (data == 2) {
            addWarningMessage(" Kindly Fill All Fileds");
          }
          if (data == 1) {
            addSuccessMessage("Page Link Updated Successfuly");
            loadData("../Database/widgets/LoadFooterMenu.php", "#footerlinkList");

            $(".modal").fadeOut(400);
          }
        },
        error: function () {
          addErrorMessage("Error 404");
        },
      });
    } else {
      addWarningMessage(" Kindly Fill All Fileds");
    }
  });

  ///////Delete Footer Menue/////////////
  $(document).on("click", ".footerMenu-delete", function () {
    let targetElement = this;
    let linkid = $(this).data("linkid");
    let title = $(this).data("title");

    function succssAction() {
      $.ajax({
        url: "../Database/widgets/deleteFooterMenu.php",
        type: "POST",
        data: { linkid: linkid },
        success: function (data) {
          if (data == 2 || data == 3) {
            addErrorMessage(" An Error Occured Kindly Try Again");
          }
          if (data == 1) {
            addSuccessMessage(" Page Link Deleted Successfully");
            $(targetElement).closest("tr").fadeOut();
          }
        },
        error: function () {
          addErrorMessage("error 404");
        },
      });
    }
    Confirmation(
      "Delete Page Link",
      "Are you sure you want to Delete " + title + " ?",
      "Yes",
      "Cancel",
      succssAction
    );
  });


  ///////////Widgets Page Ends//////////////
}); //////Documrnt End/////
