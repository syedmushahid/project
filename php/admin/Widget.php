<?php
include "config.php"
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Widget | <?php echo $title ?></title>
    <?php echo $links ?>
</head>

<body>
    <div class="col-12">
        <div class="col-lg-2 col-md-3 col-xs-7">
            <?php include "menu.php" ?>
        </div>
     <div class="col-md-9 col-lg-10" >
     <?php include "adminheader.php"?>

        <div class="col-12 Widget">
<select class="col-6" name="selectWidget" id="selectWidget">
<option selected disabled>---Select Widget---</option>
<option value="1">Top Navbars</option>
<option value="2">Logo</option>
<option value="3">Main Navbars</option>
<option value="4">Tag Line</option>
<option value="5">Categories</option>
<option value="6">Html/Text</option>
<option value="7">Footer</option>
<option value="8">Footer Bottom</option>
</select>
        </div>
        <div id="widget-WorkArea" class="col-10">
        <!-- Dynamic Data Here -->
    </div>
    </div>
</div>

</body>
<?php echo $script?>
</html>