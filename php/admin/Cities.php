<?php
include "config.php";
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Cities | <?php echo $title ?></title>
    <?php echo $links ?>
</head>

<body>
    <div class="col-12">
        <div class="col-lg-2 col-md-3 col-xs-7">
            <?php include "menu.php" ?>
        </div>
     <div class="col-md-9 col-lg-10" >
     <?php include "adminheader.php"?>



<div class="col-12 mid-boxes" >


<div id="popupBtn" class="col-md-2 col-sm-5 top-result  mousePointer"><a><i style="font-size: 30px;" class="fas fa-plus"></i><br><br>City </a>
</div>
<div class="col-md-5 searchBox">
          <input id="city-query" class="input-field"  type="text"><button id="search-city">Search</button>
        </div>

<div id="citytable"class="col-sm-11 box-bg" style="margin-left: auto;margin-right: auto;">

<?php include "../Database/LoadCity.php";?>
        </div>
</div>

     </div>
    
    </div>

    <div id="popupbg" class="modal">
  <div class="modal-content">
    <span class="close-popup">&times;</span>
    <h2 class="box-top-h">Add City</h2>
    <form id = "add-city">
      <input type="text" name="city" id="add-City" class="input-field" placeholder="Enter New City" />
    </form>
    <button id="addcity" class="button-sec">Add</button>
    <button class="cancel button-primary">Cancel</button>
  </div>
</div>

<div id="edit-city-modal" class="modal">
  <div class="modal-content">
    <span class="close-popup">&times;</span>
    <h2 class="box-top-h">Update City</h2>
    <form id = "update-city">
     
    <input type="text" name="city"  id="editcity" class="input-field" placeholder="Enter New city" />
  <input type="number" id="city-id" style="display: none;">  
  </form>
    <button id="save-edit" class="button-sec">Save</button>
    <button  class="cancel button-primary">Cancel</button>
  </div>
</div>
</body>
  <?php echo $script?>
</html>