<?php
include "config.php";
include "Modules/GetTotalNumbers.php";
?>


<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Admin | <?php echo $title ?></title>
    <?php echo $links ?>
</head>

<body>
    <div class="col-12">
        <div class="col-2">
            <?php include "menu.php" ?>
        </div>
     <div class="col-10" >
     <?php include "adminheader.php"?>



<div class="col-12 mid-boxes" >

<div class="col-md-2 col-xs-5 top-result box-bg"><a href="users.php">Workers<br><br><?php echo totalRows("User where User_Role='4'") ?></a></div>
<div class="col-md-2 col-xs-5 top-result box-bg"><a href="users.php">Customers<br><br><?php echo totalRows("User where User_Role='3'") ?></a></div>
<div class="col-md-2 col-xs-5 top-result box-bg"><a href="Services.php">Services<br><br><?php echo totalRows("services") ?></a></div>
<div class="col-md-2 col-xs-5 top-result box-bg"> <a href="categories.php">Catagories<br><br><?php echo totalRows("category") ?></a></div>
</div>
<div class="col-12 mid-boxes">

<div class="col-md-5 box-bg">
<table>
  <caption>Top Cities</caption>
  <thead>
    <tr>
      <th scope="col">Name</th>
      <th scope="col">Total Users</th>
    </tr>
  </thead>
  <tbody>
      <?php
        $query = "select * from city order by Total_Users desc limit 10";
        $run = mysqli_query($connection, $query);
        if(mysqli_num_rows($run) > 0) {  

        while($row=mysqli_fetch_assoc($run))
        {
            ?>
            <tr>
      <td data-label="Name"><?php echo$row['City_Name']?></td>
      <td data-label="Total Posts"><?php echo$row['Total_Users']?></td>
    </tr>
       <?php } }?>
  </tbody>
</table>

</div>
<div class="col-md-5 box-bg">
<table>
  <caption>Top Categories</caption>
  <thead>
    <tr>
      <th scope="col">Name</th>
      <th scope="col">Total Services</th>
    </tr>
  </thead>
  <tbody>
      <?php
        $query = "select * from category order by Total_Posts desc limit 10";
        $run = mysqli_query($connection, $query);
        if(mysqli_num_rows($run) > 0) {  

        while($row=mysqli_fetch_assoc($run))
        {
            ?>
            <tr>
      <td data-label="Name"><?php echo$row['Category_Name']?></td>
      <td data-label="Total Posts"><?php echo$row['Total_Posts']?></td>
    </tr>
       <?php } }?>
  </tbody>
</table>
</div>
</div>
<div class="col-sm-11 box-bg" style="margin-left: auto;margin-right: auto;">

<table>
  <caption>Recent Services</caption>
  <thead>
    <tr>
    <th scope="col">ID</th>
      <th scope="col">Title</th>
      <th scope="col">Category</th>
      <th scope="col">User Name</th>
    </tr>
  </thead>
  <tbody>
      <?php
        $query = "SELECT services.Service_Id AS Service_Id,
        CONCAT(user.First_Name,' ' ,user.Last_Name) AS Name,
        category.category_Name AS Category_Name, services.Title AS Title FROM services INNER JOIN
         category ON services.Category_Id=category.Category_Id INNER JOIN user 
         ON services.Worker_Id=user.id ORDER BY services.Service_Id DESC LIMIT 10";
        $run = mysqli_query($connection, $query);
        if(mysqli_num_rows($run) > 0) {  

          $rownum=0;
          $class;
        while($row=mysqli_fetch_assoc($run))
        {
            ?>
            <tr>
      <td data-label="ID"><?php echo$row['Service_Id']?></td>
      <td data-label="Title"><?php echo$row['Title']?></td>
      <td data-label="Category"><?php echo$row['Category_Name']?></td>
      <td data-label="Name"><?php echo$row['Name']?></td>

    </tr>
       <?php $rownum++; } }?>
  </tbody>
</table>
</div>

     </div>
    
    </div>

</body>
<?php echo $script?>

</html>