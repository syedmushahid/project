<?php
include "config.php";
$name = "";
$subject = "";
$message = "";
$email = "";
$err4 = "";
$err3 = "";
$err2 = "";
$err1 = "";

if ($_SERVER['REQUEST_METHOD'] === 'POST') {

    $name = $_POST['name'];
    $subject = $_POST['subject'];
    $message = $_POST['message'];
    $email = $_POST['email'];
    if (isset($name)) {
        $err4 = "Enter Your Name";
    }
    if (empty($_POST['email'])) {
        $err3 = "Enter Your Email";
    }
    if (empty($_POST['subject'])) {
        $err2 = "Enter Subject";
    }
    if (empty($_POST['message'])) {
        $err1 = "Enter Your Message";
    }
    if (!empty($_POST['name']) && !empty($_POST['email']) && !empty($_POST['subject']) && !empty($_POST['message'])) {


        $to_email = "13629@students.riphah.edu.pk";
        $subject = $_POST['subject'];
        $body = "<html>Email : " . $_POST['email'] . " <br> Name : " . $_POST['name'] . "<br>Message : " . $_POST['message'] . "<br> This Message Was Sent From Contact Us Form Of " . $title . "</html>";
        $headers = "Content-Type: text/html; charset=UTF-8\r\n";

        if (mail($to_email, $subject, $body, $headers)) {

            echo "<script>alert('Message Has Been Sent');</script>";
        } else {
            echo "<script> alert('Message Sent Failed');</script>";
        }
    }
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Contact Us | <?php echo $title ?></title>
    <?php echo $links ?>
</head>

<body>
    <?php include "header.php" ?>

    <main>
        <div class="col-md-6 col-sm-10 signup-inner-box">
            <h1 id="signup-h1">Contact Us</h1>

            <form id="contactUsForm" class="signup-form col-12" action="Contact_us.php" method="POST">
                <span style="color:red"><?php echo $err4 ?></span>
                <input class="input-field" value="<?php echo $name ?>" id="name" name="name" type="name" placeholder="Enter Name">
                <span style="color:red"> <?php echo $err3 ?></span>
                <input class="input-field" value="<?php echo $email ?>" id="email" name="email" type="email" placeholder="Enter Email">
                <span style="color:red"> <?php echo $err2 ?></span>
                <input class="input-field" value="<?php echo $subject ?>" id="subject" name="subject" type="subject" placeholder="Subject">
                <span style="color:red"> <?php echo $err1 ?></span>
                <textarea class="input-field" id="message" name="message" rows="4" cols="50" maxlength="200" placeholder="Message"><?php echo $message ?></textarea>
                <button id="contactbtn" type="submit" class="button-sec s-btn">Send Message</button>
            </form>


        </div>
    </main>
    <footer>

    </footer>
    <?php include "footer.php" ?>
</body>

</html>