<?php
include "config.php";
$error = "";
if (session_status() === PHP_SESSION_NONE) {
    session_start();
    if (!isset($_SESSION['user_email']) || empty($_SESSION['user_email'])) {
        header("location:{$hostname}");
        die();
    }
}
if (!empty($_POST['pass']) && !empty($_POST['cpass'])) {
$pass=mysqli_real_escape_string($connection,$_POST['pass']);
$cpass=mysqli_real_escape_string($connection,$_POST['cpass']);
    if ($cpass== $pass) {
        if (strlen($cpass>= 6)) {

            $sql = "Update user set Pass = '{$cpass}' where email ='{$_SESSION['user_email']}'";
            if (mysqli_query($connection, $sql)) {
                unset($_SESSION['user_email']);
                header("location:{$hostname}login.php");
            }
        } else {
            $error = "Password cant bt Smaller than 6 characters";
        }
    }
    else{
        $error = "Password Didnot Matched";
    }
}



?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Verify Email | <?php echo $title ?></title>
    <?php echo $links ?>
</head>

<body>
    <?php include "header.php"?>

    <main>
<div class="col-lg-4 col-md-6 col-xs-10 forms-box">
    <h2>New Password</h2>
    <p style="color:red"><?php echo $error ?></p>
    <form action="new-password.php" method="post">
        <input class="input-field forgot-pass" type="password" name="pass" placeholder="Enter New Password" />
        <input class="input-field forgot-pass" type="password" name="cpass" placeholder="Confirm Password" />

        <button type="submit" class="button-sec">Update</button>
    </form>
    <br>
    <br>
</div>
</main>
    <footer>

    </footer>
    <?php include "footer.php" ?>
</body>
<?php echo $script?>
</html>