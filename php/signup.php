<?php include "config.php";
if (session_status() === PHP_SESSION_NONE) {
    session_start();
}
if(isset($_SESSION['user_id']))
{
    header("location:{$hostname}");
}

$query="Select * from city";
$result=mysqli_query($connection,$query);

if (isset($_SESSION['wronginfo']) && $_SESSION['wronginfo'] == 1 && isset($_SESSION['email']) && isset($_SESSION['pass'])) {
    $error = "Wrong Email or Password";
    $email = $_SESSION['email'];
    $pass = $_SESSION['pass'];
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Create Account | <?php echo $title ?></title>
    <?php
    echo $links;
    ?>
</head>

<body>
    <?php
    include "header.php";
    ?>
    <main>

        <div class="container">
            <div class="col-lg-9 col-md-11 col-xs-12 forms-box">

                <div class="col-md-6 col-sm-10 signup-inner-box">
                    <h1 id="signup-h1">Sign up</h1>

                    <form id="signupForm" class="signup-form col-12" action="Database/AddSignUpData.php" method="POST" >
                        <input  name="fname" class="fname" id="fname" type="text" placeholder="First Name">
                        <input  name="lname" class="lname" id="lname" type="text" placeholder="Last Name">
                        <label id="lable-email" class="error" for="cpassword"></label>

                        <input class="input-field" id="email" name="email" type="email" placeholder="Enter Email" >
                        <label id="lable-pass" class="error" for="cpassword"></label>

                        <input class="input-field" id="password" name="password" type="password" placeholder="Enter Password">
                        <label id="lable-cpass" class="error" for="cpassword"></label>
                        <input class="input-field" name="cpassword" id="cpassword" type="password" placeholder="Confirm Password" onkeyup="confirmPassword()">
                       <select name="role" class="fname">
                           <option disabled selected>Who Are You?</option>
                           <option  value="3">Customer</option>
                           <option  value="4">Worker</option>
                       </select>
                       <select name="city" class="lname">
                           <option disabled selected>Select City</option>
                           <?php 
                           if(mysqli_num_rows($result)>0){
while($row=mysqli_fetch_assoc($result))
{
    ?>
    <option  value="<?php echo $row['City_Id']?>"><?php echo $row['City_Name']?></option>

<?php } }?>
                          
                          
                       </select>
                        <p style="padding-left: 10px;"> Already Have An Account? <a href="./login.php"> Login Here</a></p>
                    </form>
                    <button id="singupbtn" class="button-sec s-btn">Create Account</button>


                </div>
                <div class="col-md-6 col-sm-10  singup-options signup-inner-box">
                    <h1 id="signup-h1">OR</h1>

                    <button id="singup-with-gmail" class="button-sec s-btn google-btn">Continue With Google</button>
                    <button class="button-sec s-btn facebook-btn">Continue With Facebook</button>
                </div>
            </div>
        </div>

    </main>

    <?php
    include "footer.php";
    ?>
</body>
</html>