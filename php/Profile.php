<?php
include "config.php";
include "Modules/gettime.php";
if (isset($_GET['id'])) {
    $userid = mysqli_real_escape_string($connection,$_GET['id']);
} else {

    if (session_status() === PHP_SESSION_NONE) {
        session_start();
        if (isset($_SESSION['user_id']) && !empty($_SESSION['user_id'])) {
            $userid = $_SESSION['user_id'];
        } else {
            header("location:{$hostname}");
            die();
        }
    }
}
$query = "SELECT  media.Profile_Photo,user.Phone_Number, user.Address, category.Category_Name, user.Bio, user.Email,
CONCAT(user.First_Name,' ' ,user.Last_Name) AS Name from user 
left join category on user.Skill_Id=category.Category_Id 
left join media on user.id=media.User_Id 
where user.id={$userid}" or die("q fail");
$result = mysqli_query($connection, $query);
$getposts = "SELECT CONCAT(user.First_Name,' ' ,user.Last_Name) AS Name,services.price as Fee,
category.Category_Name AS Category,services.Service_Id As service_id,
services.Title AS Title ,services.thumbnail As thumbnail, 
services.posted_time AS posted_time,services.views as views
FROM services LEFT JOIN category ON services.Category_Id=category.Category_Id 
INNER JOIN user ON services.Worker_Id=user.id  where services.Worker_Id={$userid} ORDER by services.Service_Id DESC";
$postsresult = mysqli_query($connection, $getposts);

?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?php echo $title ?>| Profile</title>
    <?php echo $links ?>
</head>

<body>
    <?php
    include "header.php";
    ?>
    <main>
        <?php
        if (mysqli_num_rows($result) > 0) {

            while ($row = mysqli_fetch_assoc($result)) {

        ?>
                <div class="container-fluid profile-card-background">
                </div>
                <div class="container">
                    <div class="profile-card col-12">
                        <div class="profile-photo col-lg-12">
                            <?php if ($row['Profile_Photo'] == "") {
                            ?>
                                <img src="https://png.pngtree.com/png-vector/20190223/ourlarge/pngtree-profile-line-black-icon-png-image_691051.jpg" alt="profile" />
                            <?php } else {


                            ?>
                                <img src="<?php echo $row['Profile_Photo'] ?>" alt="profile" />
                            <?php } ?>
                        </div>
                        <div class=" col-lg-10 col-md-11 profile-card-body">
                            <div class="col-12">
                                <div class="col-md-12 profile-name">
                                    <h1><?php echo $row['Name'] ?></h1>
                                </div>
                                <div class="col-md-12 profile-btns"> <?php

                                                                        if (isset($_SESSION['user_id'])) {
                                                                            if ($userid == $_SESSION['user_id']) { ?>
                                            <a href="profile-settings.php"><button class="contact-number button-sec">Edit<i class="fas fa-edit"></i></button></a>
                                        <?php
                                                                            }

                                        ?>

                                        <button class="contact-number button-sec"><i class="fas fa-phone"></i>&nbsp;&nbsp;<?php echo $row['Phone_Number'] ?></button>

                                    <?php } else {

                                    ?>
                                        <a class="login-modal"> <button class="contact-number button-sec"><i class="fas fa-phone"></i>Show Phone Number</button></a>
                                    <?php
                                                                        } ?>
                                </div>


                            </div>


                            <p style="text-align: center;"><?php echo $row['Bio'] ?></p>
                            <div class="profile-address col-12">

                                <p> <i class="fas fa-envelope ProfileIcon"> </i>&nbsp;&nbsp;&nbsp;<?php echo $row['Email'] ?></p>&nbsp;
                                <p><i class="fas fa-map-marker-alt ProfileIcon"></i>&nbsp;&nbsp;&nbsp; <?php echo $row['Address'] ?></p>&nbsp;
                                <p>
                                    <i class="fas fa-user-tie ProfileIcon"></i>&nbsp;&nbsp;&nbsp;<?php echo $row['Category_Name'] ?>
                                </p>
                            </div>
                        </div>

                    </div>



                    <div class="col-12 services">
                        <?php
                        if (mysqli_num_rows($postsresult) == 0) {
                            echo "<h3 style='margin:auto;height:100px'>No Posts To Show.</h3>";
                        } else {
                            while ($post = mysqli_fetch_assoc($postsresult)) {

                        ?>
                                <div class="col-lg-4 col-md-6 col-sm-8 col-xs-10 postsBoxes">
                              <?php  if (isset($_SESSION['user_id'])) {
                               if ($userid == $_SESSION['user_id']) { ?>
                                <span  data-postid="<?php echo $post['service_id']?>" data-title="<?php echo $post['Title']?>"
       class="post-delete btn-icon delete-btn"><i class=" fas fa-trash-alt"></i></span>
       <?php }
       }?>
                                    <div class="service-card col-11">
                                        <div class="service-card-thumbnail">
                                            <a href="post.php?id=<?php echo $post['service_id']; ?>">
                                            <div class="service-card-hover col-12">
                                                    <button>Show Details</button>
                                                </div>
                                            </a>
                                                
                                            <img class="service-thumb" src="../Uploads/<?php echo $post['thumbnail'] ?>" alt="post thumbnail" />
                                        </div>

                                        <div class="service-card-body col-12">
                                            <span class="card-category-tag"><?php echo $post['Category'] ?></span>
                                            <span style="margin-left:auto;margin-top:-19px"><i class="fas fa-eye"></i>&nbsp;&nbsp;<?php echo $post['views'] ?></span>

                                            <a href="post.php?id=<?php echo $post['service_id']; ?>">
                                                <h3><?php echo $post['Title'] ?></h3>
                                            </a>
                                            <hr>
                                            <div class="service-card-user">
                                                <a href=profile.php?id=<?php echo $userid;  ?>>
                                                    <?php if ($row['Profile_Photo'] == "") {
                                                    ?>
                                                        <img src="https://png.pngtree.com/png-vector/20190223/ourlarge/pngtree-profile-line-black-icon-png-image_691051.jpg" alt="profile" />
                                                    <?php } else {


                                                    ?>

                                                        <img src=<?php echo $row['Profile_Photo'] ?> alt="profile" />

                                                    <?php } ?>
                                                </a>
                                                <div style="align-items: start;" class="col-12">
                                                    <div class="user-info col-8">
                                                        <a href=profile.php?id=<?php echo $userid;  ?>>
                                                            <h5><?php echo $post['Name'] ?></h5>
                                                        </a>
                                                        <small><?php echo time_elapsed_string($post['posted_time']); ?></small>
                                                    </div>
                                                    <div class="col-4 visiting-fee">
                                                        <p>Visiting Fee</p>
                                                        <h4><?php echo $post['Fee'] ?>/RS</h4>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>


                                    </div>

                                </div>


                <?php }
                        }
                    }
                } ?>

                    </div>


                </div>



                <?php

                if (mysqli_num_rows($result) == 0) {
                    echo " Not Found 404";
                }
                ?>


                <br>
                <br>
    </main>
    <?php include "footer.php" ?>
</body>

</html>