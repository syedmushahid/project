<?php

if (session_status() === PHP_SESSION_NONE) {
    session_start();
}
if (isset($_SESSION['user_id'])) {

    $login = true;
} else {
    $login = false;
}



?>
<header class=" container-fluid">
    <div class="top_nav col-12 container">
        <ul>
            <?php
            include "config.php";
            $pages = "SELECT * from toppages";
            $getpages = mysqli_query($connection, $pages);
            if (mysqli_num_rows($getpages) > 0) {
                while ($pagesrow = mysqli_fetch_assoc($getpages)) {

                    echo '<li><a href="' . $pagesrow['link'] . '">' . $pagesrow['title'] . '</a></li>';
                }
            }

            ?>
        </ul>
        <br />
        <div class="dropdown-wrapper">
            <div id="topusername">
                <?php if ($login == true) {
                    echo '<i class="fas fa-user"></i>&nbsp;  ' . $_SESSION['first_name'] . ' &nbsp; <i style="float: right; margin-right:5px" class="fas fa-caret-down"></i>';
                } else {
                    echo '<a href="login.php">Login</a>';
                }

                ?>
            </div>


            <div id="dropdown">
            <a href="<?php echo $hostname;?>">
                    <div class="topdropdown"><i class="fas fa-home"></i> Home</div>
                </a>
                <a href="profile.php">
                    <div class="topdropdown"><i class="fas fa-user"></i> Profile</div>
                </a>
                <?php if ($_SESSION['user_role'] == 0 || $_SESSION['user_role'] == 1 || $_SESSION['user_role'] == 2) {
                ?> <a href="admin/home.php">
                        <div class="topdropdown"><i class="fas fa-th-large"></i> Admin Dashboard</div>
                    </a>
                <?php } ?>
                <a href="profile-settings.php">
                    <div class="topdropdown"><i class="fas fa-cog"></i> Settings</div>
                </a>
                <a href="Modules/logout.php">
                    <div class="topdropdown"><i class="fas fa-sign-out-alt"></i> Logout</div>
            </div></a>
        </div>
    </div>

    <hr>

    <div class="header container col-12">

        <div class=" col-md-3 logo">
            <?php
            $logoquery = "SELECT * from logo order by id desc limit 1";
            $getlogo = mysqli_query($connection, $logoquery);
            if (mysqli_num_rows($getlogo) > 0) {
                $logorow = mysqli_fetch_assoc($getlogo);

            ?>
                <img src="<?php echo "../Uploads/Logo/" . $logorow['Logosrc'] ?>" alt="logo">
            <?php
            } else {
                echo "<h1>" . $title . "</h1>";
            }

            ?>
        </div>
        <div class="col-md-6 search-box container">

            <?php include "Modules/search.php" ?>
        </div>


    </div>

</header>
<nav class="navbar ">
    <div class="container col-12">

        <div style="display: flex;" class="col-12">
            <span style="float: left;" onclick="toggle_menu()"><i class="far fa-times-circle" id="close_menu"></i>
                <i class="fas fa-bars" id="open_menu"></i>
            </span>
            <!-- <div style="margin: auto;" id="mobileNav-logo" class="col-8 search-box ">
        
        <?php echo $title; ?>
        
        </div> -->
            <ul>



                <span id="navbar">
                    <li><a href="index.php"><b><i class="fas fa-home"></i></a></b></li>
                    <?php

                    $_query = "Select category.Category_Id as link, category.Category_Name as name from navbar inner join category on navbar.Category_Id=category.Category_Id";
                    $_result = mysqli_query($connection, $_query);
                    while ($row = mysqli_fetch_assoc($_result)) {
                    ?>
                        <li><a href="services.php?categoryId=<?php echo $row['link'] ?>"><?php echo $row['name'] ?></a></li>
                        <!-- <li><a href="#">Electrician</a></li>
                <li><a href="#">Plumber</a></li>
                <li><a href="#">Carpanter</a></li>
                <li><a href="#">Painter</a></li>
                <li><a href="#">Labour</a></li>
                <li><a href="#">PC/Mobile Repair</a></li> -->
                    <?php } ?>
                </span>

                </li>

            </ul>
            <div style="margin-left: auto;" class="mobile_login_btn">
                <?php if ($login == false) {
                ?>
                    <a class="login-modal">Add Post</a>

                <?php
                } else {

                ?>
                    <a href="post_service.php">Add Post</a></li>
                <?php }
                ?>
            </div>
        </div>

    </div>
</nav>

<div id="popupbg" class="modal col-12">
    <div class="modal-content col-lg-7 col-md-8 col-sm-10 col-xm-11">
        <span id="close-popup">&times;</span>


        <div class="col-12">

            <div class="col-lg-6 col-sm-10 signup-inner-box">
                <h1 id="signup-h1">Login</h1>

                <form id="modalloginForm" class="signup-form col-12" action="Database/LoginData.php" method="POST">

                    <input class="input-field" id="lemail" name="lemail" type="email" placeholder="Enter Email">


                    <input class="input-field" id="lpassword" name="lpassword" type="password" placeholder="Enter Password">


                    <p style="padding-left: 10px;"> Dont Have Any Account? <a href="signup.php"> Signup Here</a> <br>
                        <br>
                        <a href="Forgot-password.php">Forgot Password ?</a></>

                </form>
                <button type="submit" form="modalloginForm" class="button-sec s-btn">Login</button>


            </div>
            <div class="col-lg-6 col-sm-10  singup-options signup-inner-box">
                <h1 id="signup-h1">OR</h1>

                <button id="login-with-gmail-popup" class="button-sec s-btn google-btn">Continue With Google</button>
                <button class="button-sec s-btn facebook-btn">Continue With Facebook</button>
            </div>
        </div>





    </div>
</div>