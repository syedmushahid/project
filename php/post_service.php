<?php include "config.php";
if (session_status() === PHP_SESSION_NONE) {
    session_start();
}
if (!isset($_SESSION['user_id'])) {
    header("location:{$hostname}");
}

?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Post a Service | <?php echo $title ?></title>

    <?php echo $links ?>
</head>

<body>
    <?php include "header.php" ?>
    <main>


        <form id="add-post" method="POST" class="col-lg-6 col-md-8 col-sm-10 col-xs-11 forms-box">

            <div class="col-12 ">
                <h2 class="box-heading">Post a Service</h2>
            </div>


            <input class="input-field " style="font-weight:bold; font-size:30px" id="title" name="title" type="text" placeholder="Service Title">

            <textarea style="font-weight:500;font-size:1rem" rows="10" class="input-field" name="description" placeholder="Add Description"></textarea>

            <select class="fname" id="fname" name="category">
                <?php include "Modules/categories.php" ?>
            </select>
            <input style="font-weight:500;font-size:1rem" class=" lname" type="number" id="price" name="price" placeholder="Visiting Fee In RS">
            <div class=" tab-heading">Add Thumbnail</div>
            <input class="input-field" type="file" id="thumbnail"  name="thumbnail">

            <div class="col-12 tab-data" id="thumbnail-tab" style="background-color: white;">
                <img id="upload-img" src="../images/upload-img.png" style="margin: auto;" alt=" " />
            </div>

            <div id="uploadedthumbnail" class="col-12"></div>

            <button id="save-post" class=" button-sec post-button">Post</button>
        </form>

    </main>
    
<?php
include"footer.php";
?>
</body>


</html>