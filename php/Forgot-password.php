<?php include "config.php";
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Forgot Password | <?php echo $title ?></title>
    <?php echo $links ?>
</head>

<body>
    <?php include "header.php" ?>

    <main>

        <form class="col-lg-4 col-md-6 col-xs-10 forms-box">
            <h2>Password Recovery</h2>
<p>Verify Your Email To Reset password</p>

            <input id="email-pass-reset" class="input-field forgot-pass" type="Email" name="user_email" placeholder="Enter Your Email">
            <button id="send-new-pass-code" type="submit" class="button-sec">Send Code</button>

            <br>
            <br>
        </form>

       
    </main>
    <footer>

    </footer>
    <?php include "footer.php" ?>
</body>
</html>