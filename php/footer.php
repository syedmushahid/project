<?php include "config.php" ?>
<footer>
  <div class="container col-12">
    <!-- footer left start -->
    <div class="col-md-4">
      <!-- <h3>Mazdoor.pk</h3>
      <p>
        Mazdoor.pk was designed to help Pakistani employers with resources needed when searching for domestic staff, whether that is a driver or a high-class cook in Karachi, Lahore and Peshawar.
        <br>
        <br>
        Mazdoor.pk is the easiest way to recruit manpower on demand without dealing with expensive overhead costs. Hire people on demand in your city, <a href="#search-box"><b>Search Now</a>
      </b>
    </p> -->

    <?php include "footerleft.txt"?>
  </div>
  <!-- footer left End -->
 
  <div class="col-md-4">
    <h3>Important Link</h3>
    <?php 
    $links="Select * from footerpages";
    $getLinks=mysqli_query($connection,$links);
    if(mysqli_num_rows($getLinks)){

      while($lnk=mysqli_fetch_assoc($getLinks)){

       echo "<a href=".$lnk['link'].">".$lnk['title']."</a>";
      }
    }
    ?>
      <!-- <a href="">About</a>
      <a href="">Contact</a>
      <a href="">Privacy Policy</a>
      <a href="">Join Our Team</a>
      <a href="">DMCA</a> -->
    
    </div>
     <!-- footer Right Strat -->
     <div class="col-md-4">
       <!-- <h3>Our Office</h3>
       <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3322.5509906290613!2d72.96977871454261!3d33.616952847991634!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x38df968cf40a7279%3A0xc5f472b78a8bcfc4!2sRiphah%20International%20University!5e0!3m2!1sen!2s!4v1636105419969!5m2!1sen!2s" width="90%" style="border:0;" allowfullscreen="" loading="lazy"></iframe> -->
     <?php include "footerright.txt"?>
      </div>
       <!-- footer Right End -->
      </div>


</footer>
<div class="container-fluid main-col">
  <div class="container copyright">©2021 <?php echo $hostname; ?> All Rights Reserved</div>
</div>


<a id="scrol-top" href="#top"><i class="fas fa-arrow-alt-circle-up"></i></a>


<?php echo $script ?>