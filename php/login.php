<?php
include "config.php";
if (session_status() === PHP_SESSION_NONE) {
    session_start();
}
if (isset($_SESSION['user_id'])) {
    header("location:{$hostname}");
}
$error = "";
$email = "";
$pass = "";
$baned="";
if (isset($_SESSION['wronginfo']) && $_SESSION['wronginfo'] == 1 && isset($_SESSION['email']) && isset($_SESSION['pass'])) {
    $error = "Wrong Email or Password";
    $email = $_SESSION['email'];
    $pass = $_SESSION['pass'];
}
else if(isset($_SESSION['banned'])){
    $error ="Your Account Has Been Baned !";
}

?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Login | <?php echo $title ?></title>
    <?php echo $links ?>
</head>

<body>
    <?php include "header.php" ?>
    <main>
        <div class="container">
            <div class="col-lg-9 col-md-11 col-xs-12 forms-box">
                <div class="col-md-6 col-sm-10 signup-inner-box">
                    <h1 id="signup-h1">Login</h1>

                    <form id="loginForm" class="signup-form col-12" action="Database/LoginData.php" method="POST">
                        <p style="color:red"><?php echo $error ?></p>
                        <input class="input-field" id="lemail" name="lemail" value="<?php echo $email ?>" type="email" placeholder="Enter Email">


                        <input class="input-field" id="lpassword" value="<?php echo $pass ?>" name="lpassword" type="password" placeholder="Enter Password">


                        <p style="padding-left: 10px;"> Dont Have Any Account? <a href="signup.php"> Signup Here</a> <br>
                            <br>
                            <a href="Forgot-password.php">Forgot Password ?</a></>

                    </form>
                    <button type="submit" form="loginForm" class="button-sec s-btn">Login</button>


                </div>
                <div class="col-md-6 col-sm-10  singup-options signup-inner-box">
                    <h1 id="signup-h1">OR</h1>

                    <button id="login-with-gmail" class="button-sec s-btn google-btn">Continue With Google</button>
                    <button class="button-sec s-btn facebook-btn">Continue With Facebook</button>
                </div>
            </div>

        </div>
        <?php
        unset($_SESSION['password']);
        unset($_SESSION['email']);
        unset($_SESSION['wronginfo']);
        unset($_SESSION['banned']);
        ?>
    </main>
    <?php
    include "footer.php";
    ?>

</body>


</html>