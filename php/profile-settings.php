<?php include "config.php";

if (session_status() === PHP_SESSION_NONE) {
    session_start();
}
if (!isset($_SESSION['user_id'])) {
    header("location:{$hostname}");
}

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Account Settings | <?php echo $title ?> </title>
    <?php echo $links ?>
</head>

<body>
    <?php include "header.php" ?>
    <div class="container">
        <?php
        //$query="SELECT * from user where id={$_SESSION['user_id']} INNER JOIN ";
        $query = "SELECT * FROM user left JOIN city ON user.City_ID=city.City_ID left JOIN category ON user.Skill_ID=category.Category_ID where id={$_SESSION['user_id']}";
        $result = mysqli_query($connection, $query);


        if (mysqli_num_rows($result) == 1) {
            $data = mysqli_fetch_assoc($result)
        ?>
            <br><br>
            <form  class="col-12" id="Setting">
                <div class="col-12" style="align-items:flex-start;justify-content:space-around">
                <div class="col-md-5 ">
                <label for="fname">First Name *</label>
                <input name="fname" type="text" id="fname" value="<?php echo $data['First_Name'] ?>" class="input-field small">

                <label for="lname">Last Name *</label>
                <input name="lname" type="text" id="lname" value="<?php echo $data['Last_Name'] ?>" class="input-field small">

                <label for="email">Email *</label>
                <input name="email" type="text" id="email" value="<?php echo $data['Email'] ?>" class="input-field small">

                <label for="contact">Contact Number</label>
                <input name="contact" type="text" id="contact" value="<?php echo $data['Phone_Number'] ?>" class="input-field">
                <br><br><label for="bio">Bio</label>
                <textarea name="bio" id="" rows="6" class="input-field"><?php echo $data['Bio'] ?></textarea>
               
                <!-- <input name=" " type="text" id="address"value="<?php echo $data['id'] ?>" class="input-field"> -->
                <p>Upload Profile :  <input name="profilePhoto" type="file"></p>   
            </div>
               
           <div class="col-md-5">
           <label for="address">Address</label>
                <input name="address" type="text" id="address" value="<?php echo $data['Address'] ?>" class="input-field">
<div class="col-sm-6">
                <div class="col-12">City</div>
                <br>
                <select style="width:90%;margin-right:auto;" name="city" id="city">
                    <?php
                    include "../config.php";
                    $cities = "Select * from city";
                    $cities_result = mysqli_query($connection, $cities);
                    $cid = $data['City_Id'];
                    ?>


                    <option disabled selected>Select City</option>
                    <?php
                    if (mysqli_num_rows($cities_result) > 0) {
                        while ($row = mysqli_fetch_assoc($cities_result)) {
                    ?>
                            <option <?php if ($cid == $row['City_Id']) echo "selected" ?> value="<?php echo $row['City_Id'] ?>"><?php echo $row['City_Name'] ?></option>

                    <?php }
                    } ?>
                    <li></li>
                </select>
                </div>
                <!-- <input name="" type="text" id="address" value="<?php echo $data['City_Name'] ?>" class="input-field"> -->
                <div class="col-sm-6">
                <div class="col-6" style="margin-left: 10%;">Skills</div>
                <select style="width:90%;margin-left:auto;" name="skills" id="skills">
                    <?php
                    include("../config.php");
                    $categories = "Select * from Category";
                    $categories_result = mysqli_query($connection, $categories);
                    $cid = $data['Category_Id'];
                    ?>

                    <option disabled selected>Select Category</option>
                    <?php
                    if (mysqli_num_rows($categories_result) > 0) {
                        while ($row = mysqli_fetch_assoc($categories_result)) {
                    ?>
                            <option <?php if ($cid == $row['Category_Id']) echo "selected" ?> value="<?php echo $row['Category_Id'] ?>"><?php echo $row['Category_Name'] ?></option>

                    <?php }
                    } ?>
                    <li></li>
                </select>
                </div>
                <div class="col-12">
            <p>Location</p>
        </div>
                <div class="map-wrapper col-12">
                    <div class="col-12" style="height: 300px;" id="profile-settings-map"></div>
                    <span class="mapEditBtns" id="current-location-btn">Current Location</span>
                    <span class="mapEditBtns" id="edit-location-btn">Edit</span>
            <span class="mapEditBtns" id="canel-location-btn">Cancel</span>
            <span class="mapEditBtns" id="save-location-btn">Save</span>
            <div id="map-layer"></div>
        </div>
           </div>
                <input type="hidden" id="lat" name="lat" value=<?php echo $data['Lat']?>></input>
                <input type="hidden" id="lang" name="lang" value=<?php echo $data['Lang']?>></input>
                </div>
            </form>
        <?php } ?>

        <div class="col-12">
            <div style="margin:auto">
                <button id="saveSetting" class="button-primary">Save</button>&emsp; 
                <button id="changePasswordBtn" class="button-sec">Change Password</button>
            </div>
        </div>
        <br>
    </div>
    
    <div id="Password-edit" class="modal">
        <div class="col-md-6 col-xs-8 col-lg-5 modal-content">
            <span class="close-popup">&times;</span>
            <h1 class="box-top-h">Add New Password</h1>
            <form id="Change_Password" class="signup-form col-12">
                <input class="input-field" id="currentpassword" name="currentpassword" type="password" placeholder="Enter Current Password">
                <input class="input-field" id="newpassword" name="newpassword" type="password" placeholder="Enter New Password">
                <input class="input-field" id="confirmpassword" name="confirmpassword"  type="password" placeholder="Confirm Password"">
                <input class="input-field" id="id" name="id"  type="hidden" value=<?php echo $data['id']?>>
            </form>
            <button class="button-sec" id="changePassword">Add</button>
            <button class="button-primary cancel">Cancel</button>
        </div>
    </div>
<?php
    include "footer.php";
    ?>
</body>

</html>