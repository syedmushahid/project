<?php
include "config.php";
include "Modules/gettime.php";

$id = " ";
if (isset($_GET['categoryId']) && $_GET['categoryId']!=-1) {
    $id = mysqli_real_escape_string($connection,$_GET['categoryId']);
    $getTitle = "Select * from category where Category_Id={$id}";
$catResults = mysqli_query($connection, $getTitle);
$catName = mysqli_fetch_assoc($catResults);
$catTitle=$catName['Category_Name'];
}
else if(isset($_GET['search'])){
$catTitle=mysqli_real_escape_string($connection,$_GET['search']);
}
else{
    $catTitle="Services";
}

?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title><?php echo $title . " | " . $catTitle; ?></title>
    <?php echo $links ?>
</head>

<body>
    <?php
    include "header.php";
    ?>
    <main>

        <?php include "Modules/SearchFilter.php" ?>
        <div class="col-12 container services">

            <div class="col-12" id="showPosts">

                <?php include "Database/LoadPosts.php" ?>
            </div>

        </div>


        </div>
        <br>
        <br>
    </main>
    <?php include "footer.php" ?>
</body>

</html>