
<?php 
include "../../config.php";
$query = "select * from homepagecat order by id desc";
$run = mysqli_query($connection, $query);
$total= mysqli_num_rows($run);
?>
<html>


<table>
  <caption>Current Categories</caption>
  <thead>
    <tr>
      <th scope="col">Icon</th>
      <th scope="col">Title</th>
      <th scope="col">Link</th>
      <th scope="col"></th>
    </tr>
  </thead>
  <tbody>
  <?php
        if($total > 0) {  

        while($row=mysqli_fetch_assoc($run))
        {
         
        ?>
         <tr>
         <td data-label="Icon"><?php echo $row['icon']?></td>
         <td data-label="Title"><?php echo $row['title']?></td>
         <td data-label="Link"><?php echo $row['link']?></td>
      <td><span data-link="<?php echo $row['link']?>" data-catid="<?php echo $row['id']?>" data-title="<?php echo $row['title']?>"
      data-icon='<?php echo $row["icon"]?>' class="btn-icon edit-btn home-cat-edit"><i class="fas fa-edit"></i></span>
      <span data-catid="<?php echo $row['id']?>" data-title="<?php echo $row['title']?>" data-icon='<?php echo $row["icon"]?>'
       class="btn-icon delete-btn home-cat-delete"><i class="fas fa-trash-alt"></i></span></td>
    </tr>
    <?php 
     } }?>
  </tbody>
</table>

</html>
