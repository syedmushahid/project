
<table>
  <caption>Available Categories</caption>
  <thead>
    <tr>
      <th scope="col">Name</th>
      <th scope="col">Total Services</th>
      <th scope="col"></th>
    </tr>
  </thead>
  <tbody>
      <?php
      include "../../config.php";
      $query = "SELECT * FROM category WHERE Category_Id NOT IN (SELECT Category_Id FROM navbar)";

      $run = mysqli_query($connection, $query);
        if(mysqli_num_rows($run) > 0) {  

        while($row=mysqli_fetch_assoc($run))
        {
            ?>
            <tr>
      <td data-label="Name"><?php echo$row['Category_Name']?></td>
      <td data-label="Total Posts"><?php echo$row['Total_Posts']?></td>
      <td data-label=""><i data-id="<?php echo $row['Category_Id']?>" class="btn-icon add-btn add-navbar-link fas fa-plus"></i></td>
    </tr>
       <?php } }
       ?>
  </tbody>
</table>