
<?php 
include "../../config.php";
$query = "select * from footerpages order by id desc";
$run = mysqli_query($connection, $query);
$total= mysqli_num_rows($run);
?>
<html>


<table>
  <caption>Current Page Links</caption>
  <thead>
    <tr>
      <th scope="col">Page Title</th>
      <th scope="col">Page Link</th>
      <th scope="col"></th>
    </tr>
  </thead>
  <tbody>
  <?php
        if($total > 0) {  

        while($row=mysqli_fetch_assoc($run))
        {
         
        ?>
         <tr>
         <td data-label="Title"><?php echo $row['title']?></td>
         <td data-label="Link"><?php echo $row['link']?></td>
      <td><span data-link="<?php echo $row['link']?>" data-linkid="<?php echo $row['id']?>" data-title="<?php echo $row['title']?>"
      class="btn-icon edit-btn footerMenu-edit"><i class="fas fa-edit"></i></span>
      <span data-linkid="<?php echo $row['id']?>" data-title="<?php echo $row['title']?>"
       class="btn-icon delete-btn footerMenu-delete"><i class="fas fa-trash-alt"></i></span></td>
    </tr>
    <?php 
     } }?>
  </tbody>
</table>

</html>
