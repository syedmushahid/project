<?php
@include "../config.php";
@include "../Modules/gettime.php";
$id = " ";
if (isset($_GET['categoryId']) && $_GET['categoryId'] != -1) {
    $id = mysqli_real_escape_string($connection,$_GET['categoryId']);
} else {
    $id = "category.Category_Id";
}
if (session_status() === PHP_SESSION_NONE) {
    session_start();
}
$time = "ORDER by services.Service_Id DESC";
$fee = " ";
$city = "services.City_Id";
$searchTerm = " ";

if (isset($_GET['search'])) {
    $Search = mysqli_real_escape_string($connection,$_GET['search']);
    $searchTerm = "AND (services.title Like '%" . $Search . "%' OR services.Description Like '%" . $Search . "%'  OR category.Category_Name Like '%" . $Search . "%' OR user.First_Name Like '%" . $Search . "%' OR user.Last_Name Like '%" . $Search . "%' OR user.address Like '%" . $Search . "%' OR user.Bio Like '%" . $Search . "%') ";
}

if (isset($_GET['orderId'])) {

    switch ($_GET['orderId']) {
        case 1: {
                $time = "Order by services.Service_Id DESC";
            }
            break;
        case 2: {
                $time = "Order by services.Service_Id ASC";
            }
            break;
        case 3: {
                $time = "Order by services.views DESC";
            }
            break;
        default: {
                $time = " ";
            }
    }
}
if (isset($_GET['cityId'])) {
    $city = mysqli_real_escape_string($connection,$_GET['cityId']);
}
if (isset($_GET['priceId'])) {

    switch ($_GET['priceId']) {
        case 0: {

                $fee = " ";
            }
            break;
        case 1: {
                $fee = "AND services.Price >=0  AND services.Price<=30 ";
            }
            break;
        case 2: {

                $fee = "AND services.Price >=30  AND services.Price<=50 ";
            }
            break;
        case 3: {

                $fee = "AND services.Price >=50  AND services.Price<=100 ";
            }
            break;
        case 4: {

                $fee = "AND services.Price >=100  AND services.Price<=200 ";
            }
            break;
        case 5: {

                $fee = "AND services.Price >=200";
            }
            break;
        default: {
                $fee = " ";
            }
    }
}


$limit = 9;
if (isset($_GET['page'])) {
    $page = mysqli_real_escape_string($connection,$_GET['page']);
} else {
    $page = 1;
}
$offset = ($page - 1) * $limit;

if ($city == -1 || $city == -2) {
    $city = "services.City_Id";
}
$getposts = "SELECT CONCAT(user.First_Name,' ' ,user.Last_Name) AS Name, media.Profile_Photo AS Profile_Photo,city.City_Name as cityName,services.Price as Fee,user.Address as userAddress,
    category.Category_Name AS Category,services.Service_Id As service_id,services.Worker_Id AS workerId,
    services.Title AS Title ,services.thumbnail As thumbnail, 
    services.posted_time AS posted_time,services.views as views,user.Lat as Lat,user.Lang as Lang
    FROM services LEFT JOIN category ON services.Category_Id=category.Category_Id 
    INNER JOIN user ON services.Worker_Id=user.id left JOIN media on services.Worker_Id=media.User_Id  LEFT JOIN city on services.City_Id = city.City_Id where category.Category_Id={$id} And services.City_Id = {$city} {$fee} {$searchTerm} {$time}  Limit {$offset},{$limit}";
$postsresult = mysqli_query($connection, $getposts);

//////TO Get Total Results//////
$sql1 = "SELECT *
 FROM services LEFT JOIN category ON services.Category_Id=category.Category_Id 
 INNER JOIN user ON services.Worker_Id=user.id left JOIN media on services.Worker_Id=media.User_Id  LEFT JOIN city on services.City_Id = city.City_Id where category.Category_Id={$id} And services.City_Id = {$city} {$fee} {$searchTerm} {$time}";
$result1 = mysqli_query($connection, $sql1) or die("Query Failed.");

$total_records = 0;
if (mysqli_num_rows($result1) > 0) {

    while ($data = mysqli_fetch_assoc($result1)) {
        if (isset($_GET['cityId']) && $_GET['cityId'] == -2) {

            if (!empty($data['Lat']) && !empty($data['Lang'])) {
                if (distance($data['Lat'], $data['Lang'], $_SESSION['userLat'], $_SESSION['userLang'], " k") <= 3) {

                    $total_records++;
                }
            }
        } else {
            $total_records++;
        }
    }

?>



    <div class="col-12 container services">

        <div class="col-12"> <span class="postsFound"> Total <b><?php echo $total_records; ?></b> Posts Found </span></div>


        <?php
        while ($post = mysqli_fetch_assoc($postsresult)) {

            if (isset($_GET['cityId']) && $_GET['cityId'] == -2) {

                if (distance($post['Lat'], $post['Lang'], $_SESSION['userLat'], $_SESSION['userLang'], " k") > 3) {

                    continue;
                }
            }
        ?>
            <div class="col-lg-4 col-md-6 col-sm-8 col-xs-10 postsBoxes">
                <div class="service-card col-11">
                    <div class="service-card-thumbnail">
                        <a href="post.php?id=<?php echo $post['service_id']; ?>">
                            <div class="service-card-hover col-12">
                                <button>Show Details</button>
                            </div>
                        </a>
                        <img class="service-thumb" src="../Uploads/<?php echo $post['thumbnail'] ?>" alt="post thumbnail" />
                    </div>
                    <div class="service-card-body col-12">
                        <span class="card-category-tag"><?php echo $post['Category'] ?></span>
                        <span style="margin-left:auto;margin-top:-19px"><i class="fas fa-eye"></i>&nbsp;&nbsp;<?php echo $post['views'] ?></span>
                        <a href="post.php?id=<?php echo $post['service_id']; ?>">
                            <h2><?php echo $post['Title'] ?></h2>
                        </a>
                        <div>
                            <p style="margin-bottom: 0px; font-size:15px"><i class="fas fa-map-marker-alt"></i>&nbsp;<?php echo $post['cityName'] ?> </p>
                            <p style="margin-top:4px; font-size:15px"><?php echo $post['userAddress'] ?> </p>
                        </div>
                        <hr>
                        <div class="service-card-user">
                            <a href=profile.php?id=<?php echo $post['workerId'] ?>>
                                <?php if ($post['Profile_Photo'] == "") {
                                ?>
                                    <img src="https://png.pngtree.com/png-vector/20190223/ourlarge/pngtree-profile-line-black-icon-png-image_691051.jpg" alt="profile" />
                                <?php } else {


                                ?>

                                    <img src=<?php echo $post['Profile_Photo'] ?> alt="profile" />

                                <?php } ?>
                            </a>
                            <div style="align-items: start;" class="col-12">
                                <div class="user-info col-8">
                                    <a style="width:100%" href=profile.php?id=<?php echo $post['workerId'] ?>>
                                        <h5 ><?php echo $post['Name'] ?></h5>
                                    </a>
                                   
                                    <small><?php echo time_elapsed_string($post['posted_time']); ?></small>

                                </div>
                                <div class="col-4 visiting-fee">
                                    <p>Visiting Fee</p>
                                    <h4><?php echo $post['Fee'] ?>/RS</h4>
                                </div>
                            </div>
                        </div>
                    </div>


                </div>

            </div>


        <?php

        } ?>

    </div>
<?php

    // show pagination



    $total_page = ceil($total_records / $limit);

    echo '<div class="pagination col-12"> <div>';
    if ($page > 1) {
        echo '<span><a href="services.php?categoryId=' . $id . '&page=' . ($page - 1) . '">Prev</a></span>';
    }
    for ($i = 1; $i <= $total_page; $i++) {
        if ($i == $page) {
            $active = "pagination-active";
        } else {
            $active = "";
        }
        echo '<span class="' . $active . '"><a href="services.php?categoryId=' . $id . '&page=' . $i . '">' . $i . '</a></span>';
    }
    if ($total_page > $page) {
        echo '<span><a href="services.php?categoryId=' . $id . '&page=' . ($page + 1) . '">Next</a></span>';
    }

    echo '</div></div>';
}


function distance($lat1, $lon1, $lat2, $lon2, $unit)
{
    if (($lat1 == "") || ($lat2 == "") || ($lon1 == "") || ($lon2 == "")) {

        return 1000;
    } else {
        $theta = $lon1 - $lon2;
        $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +  cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
        $dist = acos($dist);
        $dist = rad2deg($dist);
        $miles = $dist * 60 * 1.1515;
        $unit = strtoupper($unit);

        if ($unit == "K") {
            return ($miles * 1.609344);
        } else if ($unit == "N") {
            return ($miles * 0.8684);
        } else {
            return $miles;
        }
    }
}
?>