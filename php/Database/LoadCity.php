<?php 
include "../admin/config.php";
$search="";
if(isset($_GET['search'])){
  $search="Where City_Name Like '%".mysqli_real_escape_string($connection,$_GET['search'])."%'";

}

$query1 = "select * from city {$search}"; ///First Get Total Number of rows//////
$run1 = mysqli_query($connection,$query1);
$total_records = mysqli_num_rows($run1);
$limit = 15;///// number of rows to show in one page
    if (isset($_GET['page'])) {
        $page = mysqli_real_escape_string($connection,$_GET['page']);
    } else {
        $page = 1;
    }
    $offset = ($page - 1) * $limit;
    $total_page = ceil($total_records / $limit);


  $query = "SELECT * FROM city {$search} order by City_Id DESC LIMIT {$offset},{$limit}";
  $run = mysqli_query($connection, $query);
?>
<table>
  <caption>All Cities (<?php echo $total_records?>)</caption>
  <thead>
    <tr>
      <th scope="col">City Id</th>
      <th scope="col">City Name</th>
      <th scope="col">Total Users</th>
      <th scope="col">Total Services</th>
      <th scope="col"></th>
    </tr>
  </thead>
  <tbody>
      <?php

        if(mysqli_num_rows($run) > 0) {  

        while($row=mysqli_fetch_assoc($run))
        {
            ?>
            <tr>
      <td data-label="City Id"><?php echo$row['City_Id']?></td>
      <td data-label="City Name"><a target_blank href=<?php echo $hostname."services.php?cityId=".$row['City_Id'];?>><?php echo$row['City_Name']?></a></td>
      <td data-label="Total Users"><?php echo$row['Total_Users']?></td>
      <td data-label="Total Services"><?php echo$row['Total_Services']?></td>
      <td><span class="btn-icon edit-btn"><i  data-id="<?php echo $row['City_Id']?>"  data-name="<?php echo $row['City_Name']?>" class="city-edit fas fa-edit"></i></span>
      <span class="btn-icon delete-btn"><i   data-id="<?php echo $row['City_Id']?>"  data-name="<?php echo $row['City_Name']?>" class="city-delete fas fa-trash-alt"></i></span></td>
     
    </tr>
       <?php } }?>
  </tbody>
</table>
<?php
      echo '<div class="pagination col-12"> <div>';
      if ($page > 1) {
          echo '<span><a href="cities.php?page=' . ($page - 1) . '">Prev</a></span>';
      }
      for ($i = 1; $i <= $total_page; $i++) {
          if ($i == $page) {
              $active = "pagination-active";
          } else {
              $active = "";
          }
          echo '<span class="' . $active . '"><a href="cities.php?page=' . $i . '">' . $i . '</a></span>';
      }
      if ($total_page > $page) {
          echo '<span><a href="cities.php?page=' . ($page + 1) . '">Next</a></span>';
      }
  
      echo '</div></div>';
  ?>
