<?php

include "../admin/config.php";
$search = "";
if (isset($_GET['search'])) {
    $search = "AND((Phone_Number Like '%" . mysqli_real_escape_string($connection, $_GET['search']) . "%' ) OR (CONCAT(First_Name,Last_Name) Like'%" . mysqli_real_escape_string($connection, $_GET['search']) . "%') OR (Email Like '%" . mysqli_real_escape_string($connection, $_GET['search']) . "%') OR( First_Name Like '%" . mysqli_real_escape_string($connection, $_GET['search']) . "%') OR (Last_Name Like '%" . mysqli_real_escape_string($connection, $_GET['search']) . "%'))";
}

$query1 = "SELECT user.* , city.City_Name
FROM user
LEFT JOIN city
ON user.City_Id=city.City_ID WHERE ((user.User_Role=3) OR (user.User_Role=4 )){$search}";
$run1 = mysqli_query($connection, $query1);
$total_records = mysqli_num_rows($run1);
$limit = 15;
if (isset($_GET['page'])) {
    $page = mysqli_real_escape_string($connection, $_GET['page']);
} else {
    $page = 1;
}
$offset = ($page - 1) * $limit;

$total_page = ceil($total_records / $limit);
?>
<table>
    <caption>ALL USERS (<?php echo $total_records ?>)</caption>
    <thead>
        <tr>
            <th scope="col">First Name</th>
            <th scope="col">Last Name</th>
            <th scope="col">Email</th>
            <th scope="col">City</th>
            <th scope="col">Phone Number</th>
            <th scope="col"></th>

        </tr>
    </thead>
    <tbody>
        <?php
        $query = "SELECT user.* , city.City_Name
        FROM user
        LEFT JOIN city
        ON user.City_Id=city.City_ID WHERE  ((user.User_Role=3) OR (user.User_Role=4 )){$search}
        LIMIT {$offset},{$limit}";
        $run = mysqli_query($connection, $query);
        if (mysqli_num_rows($run) > 0) {

            while ($row = mysqli_fetch_assoc($run)) {
        ?>
                <tr>
                    <td data-label="First Name"><?php echo $row['First_Name'] ?></td>
                    <td data-label="Last Name"><?php echo $row['Last_Name'] ?></td>
                    <td data-label="Email"><?php echo $row['Email'] ?></td>
                    <td data-label="City"><?php echo $row['City_Name'] ?></td>
                    <td data-label="Phone Number"><?php echo $row['Phone_Number'] ?></td>
                    <td>
                        <?php
                        echo $row['banStatus'];
                        if($row['banStatus'] == "unban") {
                            ?>
                        <span data-id="<?php echo $row['id']?>" data-ban="<?php echo $row['banStatus']?>" class="update_ban">
                        <i class="btn-icon delete-btn fas fa-lock"></i></span>
                        <?php
                        }
                        else {
                            ?>
                        <span data-id="<?php echo $row['id']?>" data-ban="<?php echo $row['banStatus']?>" class="update_ban">
                        <i class="btn-icon edit-btn fas fa-lock-open"></i></span>
                        <?php } ?>
                    </td>
                </tr>
        <?php }
        }
      ?>
    </tbody>
</table>
<?php
echo '<div class="pagination col-12"> <div>';
if ($page > 1) {
    echo '<span><a href="users.php?page=' . ($page - 1) . '">Prev</a></span>';
}
for ($i = 1; $i <= $total_page; $i++) {
    if ($i == $page) {
        $active = "pagination-active";
    } else {
        $active = "";
    }
    echo '<span class="' . $active . '"><a href="users.php?page=' . $i . '">' . $i . '</a></span>';
}
if ($total_page > $page) {
    echo '<span><a href="users.php?page=' . ($page + 1) . '">Next</a></span>';
}

echo '</div></div><br><br>';
?>