<?php

if (session_status() === PHP_SESSION_NONE) {
    session_start();
}

if ((!empty($_POST['userLang']) && !empty($_POST['userLat']))) {
    $_SESSION['userLang'] = $_POST['userLang'];
    $_SESSION['userLat'] = $_POST['userLat'];
}

