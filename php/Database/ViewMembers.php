<?php
include "../Config.php";
?>


<table>
  <caption>Table Title</caption>
  <thead>
    <tr>
      <th scope="col">ID</th>
      <th scope="col">First Name</th>
      <th scope="col">Last Name</th>
      <th scope="col">Email</th>
      <th scope="col">Role</th>
    </tr>
  </thead>
  <tbody>
  <?php
  $query = "select * from user";
  $run = mysqli_query($connection, $query);

  if (mysqli_num_rows($run) >0)
  {
      while($row=mysqli_fetch_assoc($run))
      {
            ?>
              <tr>
              <td data-label="ID"><?php echo $row['id']?></td>
              <td data-label="First Name"><?php echo $row['First_Name']?></td>
              <td data-label="Last Name"><?php echo $row['Last_Name']?></td>
              <td data-label="Email"><?php echo $row['Email']?></td>
              <td data-label="Role"><?php echo $row['User_Role']?></td>
              </tr>
              <?php
      }
    }
              ?>
  </tbody>
</table>