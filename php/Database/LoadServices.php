<?php
include "../admin/config.php";
$search = "";
if (isset($_GET['search'])) {
    $search = "where (services.Title Like '%" . mysqli_real_escape_string($connection, $_GET['search']) . "%')";
}
$query1="SELECT * FROM services {$search}"; ///First Get Total Number of rows//////
$run1 = mysqli_query($connection,$query1);
$total_records=mysqli_num_rows($run1);
$limit = 15;///// number of rows to show in one page
    if (isset($_GET['page'])) {
        $page = $_GET['page'];
    } else {
        $page = 1;
    }
    $offset = ($page - 1) * $limit;
    
    $total_page = ceil($total_records / $limit);

$query = "SELECT services.Title, services.Service_Id,user.Email, user.First_Name, user.Last_Name, category.Category_Name, city.City_Name
FROM services
Left JOIN user ON services.Worker_Id=user.id 
Left JOIN category ON services.Category_Id=category.Category_Id 
Left JOIN city ON services.City_Id=city.City_Id  {$search}
LIMIT {$offset},{$limit}";
  $run = mysqli_query($connection, $query);

?>


<table>
  <caption>All Services (<?php echo $total_records ?>)</caption>
  <thead>
    <tr>
      <th scope="col">Service Id</th>
      <th scope="col">Title</th>
      <th scope="col">First name</th>
      <th scope="col">Last name</th>
      <th scope="col">City</th>
      <th scope="col">Category</th>

      <th scope="col"></th>
    </tr>
  </thead>
  <tbody>
      <?php
    
        if(mysqli_num_rows($run) > 0) {  

        while($row=mysqli_fetch_assoc($run))
        {
            ?>
            <tr>
      <td data-label="Service Id"><?php echo$row['Service_Id']?></td>
      <td data-label="Title"><a target="_blank" href=<?php echo $hostname."post.php?id=".$row['Service_Id'];?>><?php echo$row['Title']?></a></td>
      <td data-label="First Name"><?php echo$row['First_Name']?></td>
      <td data-label="Last Name"><?php echo$row['Last_Name']?></td>
      <td data-label="City"><?php echo$row['City_Name']?></td>
      <td data-label="Category"><?php echo$row['Category_Name']?></td>
      <input type="hidden" id=<?php echo "posttitle".$row['Service_Id']?> name="title" value="<?php echo $row['Title']?>">
      <input type="hidden" id=<?php echo "email".$row['Service_Id']?> name="email" value="<?php echo $row['Email']?>">
      <td><span data-id="<?php echo $row['Service_Id']?>" class="btn-icon delete-btn service-delete">
      <i class="fas fa-trash-alt"></i></span></td>
    </tr>
       <?php } }?>
  </tbody>
</table>
<?php
echo '<div class="pagination col-12"> <div>';
    if ($page > 1) {
        echo '<span><a href="Services.php?page=' . ($page - 1) . '">Prev</a></span>';
    }
    for ($i = 1; $i <= $total_page; $i++) {
        if ($i == $page) {
            $active = "pagination-active";
        } else {
            $active = "";
        }
        echo '<span class="' . $active . '"><a href="Services.php?page=' . $i . '">' . $i . '</a></span>';
    }
    if ($total_page > $page) {
        echo '<span><a href="Services.php?page=' . ($page + 1) . '">Next</a></span>';
    }

    echo '</div></div><br><br>';
?>