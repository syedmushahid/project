
<?php 
include "../admin/config.php";

$search="";
if(isset($_GET['search'])){
  $search="Where Category_Name Like '%".mysqli_real_escape_string($connection,$_GET['search'])."%'";

}

$query1="SELECT * FROM category {$search}";
$run1 = mysqli_query($connection, $query1);
$total_records = mysqli_num_rows($run1);
$limit = 15;
if (isset($_GET['page'])) {
  $page = mysqli_real_escape_string($connection,$_GET['page']);
} else {
  $page = 1;
}
$offset = ($page - 1) * $limit;

$query = "SELECT * FROM category {$search} order by Category_Id DESC   Limit {$offset},{$limit}";



$run = mysqli_query($connection, $query);
$table="";
$table='<table>
  <caption>All Categories ('.$total_records.')</caption>
  <thead>
    <tr>
      <th scope="col">ID</th>
      <th scope="col">Name</th>
      <th scope="col">Total Services</th>
      <th scope="col"></th>
    </tr>
  </thead>
  <tbody>';
        if($total_records > 0) {  

        while($row=mysqli_fetch_assoc($run))
        {
       
         $table.= '<tr>
         <td data-label="ID">'.$row["Category_Id"].'</td>
        <td data-label="Name"><a target="_blank" href="'.$hostname.'services.php?categoryId='.$row["Category_Id"].'">'.$row["Category_Name"].'</a></td>
         <td data-label="Total Posts">'.$row["Total_Posts"].'</td>
      <td><span data-id="'.$row["Category_Id"].'"'.'data-name="'.$row["Category_Name"].'"'.'
      class="btn-icon edit-btn category-edit"><i class="fas fa-edit"></i></span>
      <span data-id="'.$row["Category_Id"].'"'.'data-name="'.$row["Category_Name"].'"'.'
       class="btn-icon delete-btn category-delete"><i class="fas fa-trash-alt"></i></span></td>
    </tr>';
     } }
     $table.='
  </tbody>
</table>';
echo $table;

$total_page = ceil($total_records / $limit);
echo '<div class="pagination col-12"> <div>';
if ($page > 1) {
    echo '<span><a href="categories.php?page=' . ($page - 1) . '">Prev</a></span>';
}
for ($i = 1; $i <= $total_page; $i++) {
    if ($i == $page) {
        $active = "pagination-active";
    } else {
        $active = "";
    }
    echo '<span class="' . $active . '"><a href="categories.php?page=' . $i . '">' . $i . '</a></span>';
}
if ($total_page > $page) {
    echo '<span><a href="categories.php?page=' . ($page + 1) . '">Next</a></span>';
}
echo '</div></div><br><br>';
?>