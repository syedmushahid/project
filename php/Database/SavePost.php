<?php
include "../config.php";
if (!session_status() === PHP_SESSION_NONE) {
    header("location:{$hostname}");

    die();
} else {

    session_start();


    if (
        empty($_POST['title']) || empty($_POST['category']) || empty($_POST['description'])
        || !isset($_POST['price'])
    ) {

        echo "0";
        die();
    } else {
        $title = mysqli_real_escape_string($connection,  $_POST['title']);
        $description = mysqli_real_escape_string($connection, $_POST['description']);
        $categry = mysqli_real_escape_string($connection,$_POST['category']);
        $workerId = mysqli_real_escape_string($connection,$_SESSION['user_id']);
        $price = mysqli_real_escape_string($connection,$_POST['price']);
        $thumbnail = $_SESSION['thumbnail'];
        $insert = "INSERT INTO Services (Worker_Id,Title,Category_Id,Description,Price,thumbnail,City_Id)
VALUES ({$workerId}, '{$title}',{$categry},'{$description}',{$price},'{$thumbnail}',(SELECT City_Id from user where id={$workerId}))";
        $updateCategory = "UPDATE category set Total_Posts = Total_Posts+1 where Category_Id = {$categry}";
        $updateCity = "UPDATE city set Total_Services = Total_Services+1 where City_Id = (SELECT City_Id from user where id={$workerId})";

        if (mysqli_query($connection, $insert)) {
            mysqli_query($connection, $updateCategory);
            mysqli_query($connection, $updateCity);
            echo "1";
        } else {
            echo "2";
        }
        $_SESSION['thumbnail']="";
    }
}
