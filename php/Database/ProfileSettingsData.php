<?php

include "../config.php";
if (session_status() === PHP_SESSION_NONE) {
    session_start();
}
if (!isset($_SESSION['user_id'])) {
    header("location:{$hostname}");
}

if ($_FILES['profilePhoto']['size'] != 0) {
    $profilePhotoName = $_FILES['profilePhoto']['name'];
    $profilePhotoSize = $_FILES['profilePhoto']['size'];
    $profilePhotoType = $_FILES['profilePhoto']['type'];
    $profilePhotoTmp = $_FILES['profilePhoto']['tmp_name'];
    $filename = pathinfo($_FILES['profilePhoto']['name'], PATHINFO_FILENAME);
    $profilePhotoExt = strtolower(pathinfo($_FILES['profilePhoto']['name'], PATHINFO_EXTENSION));
    $extentions = array("jpeg", "jpg", "png");

    if (in_array($profilePhotoExt, $extentions) === false) {
        echo 4;
        die();
    }
    if ($profilePhotoSize >    2e+6) {
        echo 5;
        die();
    } else {

        $increment = 0;
        while (file_exists("../../Uploads/" . $profilePhotoName)) {
            $increment++;
            $profilePhotoName = $filename . $increment . "." . $profilePhotoExt;
        }
        if (move_uploaded_file($profilePhotoTmp, "../../Uploads/" . $profilePhotoName)) {

            $sql = "select * from media where User_Id={$_SESSION['user_id']}";
            $runit = mysqli_query($connection, $sql);
            $path = "../Uploads/" . $profilePhotoName;
            if (mysqli_fetch_assoc($runit) > 0) {
                $sql1 = "update media set Profile_Photo ='{$path}' where User_Id={$_SESSION['user_id']}";
                mysqli_query($connection, $sql1);
            } else {
                $sql1 = "Insert into media (Profile_Photo,User_Id) Values ('{$path}',{$_SESSION['user_id']})";
                mysqli_query($connection, $sql1);
            }
        }
    }
}

if (empty($_POST['fname']) || empty($_POST['lname']) || empty($_POST['email'])) {
    echo "0";
    die();
} else {
    $FName = mysqli_real_escape_string($connection,  $_POST['fname']);
    $LName = mysqli_real_escape_string($connection, $_POST['lname']);
    $Email = mysqli_real_escape_string($connection, $_POST['email']);
    $ContactNumber = mysqli_real_escape_string($connection, $_POST['contact']);
    $Address = mysqli_real_escape_string($connection, $_POST['address']);
    $City = "";
    $Skills = "";
    if (isset($_POST['skills'])) {

        $Skills = mysqli_real_escape_string($connection, $_POST['skills']);
    }
    if (isset($_POST['city'])) {

        $City = mysqli_real_escape_string($connection, $_POST['city']);
    }
    $Bio = mysqli_real_escape_string($connection, $_POST['bio']);
    $lat = $_POST['lat'];
    $lang = $_POST['lang'];

    $query = "SELECT * from User where Email='{$Email}'";
    $result = mysqli_query($connection, $query);

     $insert = "UPDATE user set First_Name='{$FName}',Last_Name='{$LName}',Email='{$Email}', Phone_Number='{$ContactNumber}', Address='{$Address}', City_Id='{$City}', Skill_Id='{$Skills}', Bio='{$Bio}',Lat ='{$lat}',Lang='{$lang}' where id={$_SESSION['user_id']}";
    // update Session Also here
    if($City!="")
    {

        $updatecity = "Update city set Total_Users = Total_Users+1 where City_Id ={$City}";
        mysqli_query($connection, $updatecity);
    }

    if (mysqli_query($connection, $insert)) {
        echo "1";
    } else {
        echo "2";
    }
    mysqli_close($connection);
}
