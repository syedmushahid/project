<?php

if (!empty($_POST['token'])) {
  require_once '../../vendor/autoload.php';
  $id_token = $_POST['token'];
  $CLIENT_ID = "1028967532523-7a1h4tnbmk0atjbbpt1bf1hcfhg36ncq.apps.googleusercontent.com";
  $client = new Google_Client(['client_id' => $CLIENT_ID]);  // Specify the CLIENT_ID of the app that accesses the backend
  $payload = $client->verifyIdToken($id_token);

  if ($payload) {
    $userid = $payload['sub'];
    $fname = $payload['given_name'];
    $lname = $payload['family_name'];
    $email = $payload['email'];
    $profilePhoto = $payload['picture'];
    include "../config.php";
    $query = "SELECT * from user where Email ='{$email}'";
    $result = mysqli_query($connection, $query);
    if (mysqli_num_rows($result) > 0) {
      $row = mysqli_fetch_assoc($result);
      if (($row['Google_Id'] != NULL) && ($row['Google_Id'] == $userid)) {

        session_start();
        $_SESSION['user_id'] = $row['id'];
        $_SESSION['first_name'] = $row['First_Name'];
        $_SESSION['user_role'] = $row['User_Role'];
        if ($_SESSION['user_role'] == 3 || $_SESSION['user_role'] == 4) {
          echo 0; //////Login As User
          die();
        } else {
          echo 1; ////Login As Admin
          die();
        }
      } else {
        echo 2; ///Use Password To Login///
      }
    } else {
      $insert = "INSERT into user (Google_Id,First_Name,Last_Name,email,User_Role,Verification) VALUES ({$userid},'{$fname}','{$lname}','{$email}',3,1)";
      $result = mysqli_query($connection, $insert);
      $newID = "SELECT * from user where Google_Id={$userid} OR email ='{$email}'";
      $getID = mysqli_query($connection, $newID);
      $data = mysqli_fetch_assoc($getID);

      session_start();
      $_SESSION['user_id'] = $data['id'];
      $_SESSION['first_name'] = $data['First_Name'];
      $_SESSION['user_role'] = $data['User_Role'];

      $insertProfile = "INSERT into media (User_Id,Profile_Photo) Values ({$data['id']},'{$profilePhoto}')";
      mysqli_query($connection, $insertProfile);
      echo 3; ////New Account Created With Default Role Customer
      die();
    }
  } else {
    echo 4; //////Failed To Verify Token
  }
} else {
  echo 5; /////Post Token Is Empety
}
mysqli_close($connection);
