<?php
include "../admin/config.php";
$search="";
if(isset($_GET['search'])){
  $search="AND( (CONCAT(First_Name,Last_Name) Like'%".mysqli_real_escape_string($connection,$_GET['search'])."%') OR (Email Like '%".mysqli_real_escape_string($connection,$_GET['search'])."%') OR( First_Name Like '%".mysqli_real_escape_string($connection,$_GET['search'])."%') OR (Last_Name Like '%".mysqli_real_escape_string($connection,$_GET['search'])."%'))";

}
$total_records=1;
$offset="";
$total_page="";
$page="";
if(!isset($_POST['Role']))
{
   $query1="SELECT * FROM user WHERE ((User_Role = 0) Or( User_Role = 1) or( User_Role = 2)) {$search}"; ///First Get Total Number of rows//////
  $run1 = mysqli_query($connection,$query1);
  $total_records=mysqli_num_rows($run1);
  $limit = 10;
 
    if (isset($_GET['page'])) {
        $page = mysqli_real_escape_string($connection,$_GET['page']);
    } else {
        $page = 1;
    }
    $offset = ($page - 1) * $limit;
    $total_page = ceil($total_records / $limit);
 $sql = "SELECT * FROM user WHERE ((User_Role = 0) Or( User_Role = 1) or( User_Role = 2)) {$search} Order By id desc LIMIT {$offset},{$limit}";
}
else{
  // $query1="select * from user"; ///First Get Total Number of rows//////
  // $run1 = mysqli_query($connection,$query1);
  // $total_records=mysqli_num_rows($run1);
  // $limit = 20;
  //   if (isset($_GET['page'])) {
  //       $page = mysqli_real_escape_string($connection,$_GET['page']);
  //   } else {
  //       $page = 1;
  //   }
  //   $offset = ($page - 1) * $limit;
  //   $total_page = ceil($total_records / $limit);
  $Role = mysqli_real_escape_string($connection,$_POST['Role']);
  $sql = "SELECT * FROM user WHERE User_Role = '{$Role}'";
}
$result = mysqli_query($connection, $sql) or die("SQL Query Failed.");

$output = "";

if(mysqli_num_rows($result) > 0 ){
  $output .= '<table border="0" width="100%"  cellpadding="10px">
              <caption>
              Team Members ('.$total_records.')
              </caption>
              <tr>
                <th width="60px">Id</th>
                <th width="90px">Name</th>
                <th width="90px">Email</th>
                <th width="60px">Role</th>
                <th width="60px">Edit</th>
              </tr>';
  while($row = mysqli_fetch_assoc($result)){
    $role= "";
    if($row['User_Role'] == 0)
    {
        $role = "Admin";
    }
    else if($row['User_Role'] == 1)
    {
      $role = "Manager";
    }
    else if($row['User_Role'] == 2)
    {
      $role = "Moderator";
    }
    $output .= '<tr>
                  <td align="center">'.$row["id"].'</td>
                  <td><a target="_blank" href="'.$hostname.'profile.php?Id='.$row["id"].'">'.$row["First_Name"].'&nbsp'.$row["Last_Name"].'</a></td>
                  <td align="center">'.$row["Email"].'</td>
                  <td align="center">'.$role.'</td>
                  <td><span data-id="'.$row["id"].'"'.'data-role="'.$row["User_Role"].'"'.'
                  class="btn-icon edit-btn userRole-edit"><i class="fas fa-edit"></i></span></td>
                </tr>';
  }    
   $output .= "</table>";

   echo $output;

   echo '<div class="pagination col-12"> <div>';
   if ($page > 1) {
       echo '<span><a href="teammembers.php?page=' . ($page - 1) . '">Prev</a></span>';
   }
   for ($i = 1; $i <= $total_page; $i++) {
       if ($i == $page) {
           $active = "pagination-active";
       } else {
           $active = "";
       }
       echo '<span class="' . $active . '"><a href="teammembers.php?page=' . $i . '">' . $i . '</a></span>';
   }
   if ($total_page > $page) {
       echo '<span><a href="teammembers.php?page=' . ($page + 1) . '">Next</a></span>';
   }
   echo '</div></div>';



}else{
    echo "No Record Found.";
}

?>

