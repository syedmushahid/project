<?php
$catid = "-1";
$cityid = "-1";
$orderid = "1";
$priceid = "0";
if (isset($_GET['categoryId'])) {
    $catid =  mysqli_real_escape_string($connection,$_GET['categoryId']);
}
if (isset($_GET['cityId'])) {
    $cityid =  mysqli_real_escape_string($connection,$_GET['cityId']);
}
if (isset($_GET['orderId'])) {
    $orderid = mysqli_real_escape_string($connection,$_GET['orderId']);
}
if (isset($_GET['priceId'])) {
    $priceid = $_GET['priceId'];
}

?>
<div class="container-fluid posts-filter-wrapper">

    <div class=" col-12 container posts-filter">
        <div class="col-lg-2 col-md-7 col-sm-8  filter-item">
            <span>Search Filter <i class="fas fa-sort-amount-down-alt"></i></span>
        </div>
        <div class="col-lg-2 col-md-7 col-sm-8  filter-item">

            <select class="filterSelect" name="showByTime" id="timeList">
                <?php echo ($orderid == "1") ? "<option selected  value='1'>Latest</option>" : "<option value='1'>Latest</option> "; ?>
                <?php echo ($orderid == "2") ? "<option selected  value='2'>Oldest</option>" : "<option value='2'>Oldest</option> "; ?>
                <?php echo ($orderid == "3") ? "<option selected  value='3'>Most viewed</option>" : "<option value='3'>Most viewed</option> "; ?>
            </select>
        </div>
        <div class="col-lg-2 col-md-7 col-sm-8  filter-item">

            <select class="filterSelect" name="showByCategory" id="CategoryList">

                <?php include "Modules/categories.php" ?>

            </select>
        </div>
        <div class="col-lg-2 col-md-7 col-sm-8  filter-item">

            <select class="filterSelect" name="showByCity" id="cityList">

                <?php include "Modules/cities.php" ?>

            </select>
        </div>
        <div class="col-lg-2 col-md-7 col-sm-8  filter-item">

            <select class="filterSelect" name="showByFee" id="priceList">
                <?php echo ($priceid == "0") ? '<option  selected  value="0">Any Visiting Fee</option>' : '<option value="0">Any Visiting Fee</option>'; ?>
                <?php echo ($priceid == "1") ? '<option selected value="1">0Rs - 30Rs </option>' : '<option  value="1">0Rs - 30Rs </option>'; ?>
                <?php echo ($priceid == "2") ? '<option selected value="2">30Rs - 50Rs </option>' : '<option value="2">30Rs - 50Rs </option>'; ?>
                <?php echo ($priceid == "3") ? '<option selected value="3">50Rs - 100Rs </option>' : '<option value="3">50Rs - 100Rs </option>'; ?>
                <?php echo ($priceid == "4") ? '<option selected value="4">100Rs - 200Rs </option>' : '<option value="4">100Rs - 200Rs </option>'; ?>
                <?php echo ($priceid == "5") ? '<option selected value="5">Morethan 200Rs  </option>' : '<option value="5">Morethan 200Rs </option>'; ?>

            </select>
        </div>

        <button class="col-lg-2 col-md-7 col-sm-8 filter-item" id="applyFilters"> Apply</button>



    </div>
</div>